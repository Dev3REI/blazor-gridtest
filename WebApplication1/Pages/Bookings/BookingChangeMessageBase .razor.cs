﻿
//https://docs.microsoft.com/en-us/aspnet/core/blazor/event-handling?view=aspnetcore-3.1


using Microsoft.AspNetCore.Components;
using Microsoft.VisualBasic;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Navigations;
using Syncfusion.Blazor.Popups;
using Syncfusion.Blazor.Schedule;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Pages.BookingServices;

namespace WebApplication1.Pages.Bookings
{
    public class BookingChangeMessageBase : ComponentBase
    {
        [Parameter]
        public bool DialogVisible { get; set; } = false;
        [Parameter]
        public string SimplePopupHeader { get; set; } = "REIMaster";
        [Parameter]
        public string SimplePopupContent { get; set; } = "REIMaster";
        [Parameter]
        public string SimplePopupImage { get; set; } = "fa-warning";
        [Parameter]
        public bool SimplePopupShowCloseButton { get; set; } = false;
        [Parameter]
        public bool SimplePopupIsBookingChangeMessage { get; set; } = true;
        [Parameter]
        public string SimplePopupHeaderBackground { get; set; } = "cornflowerblue";
        [Parameter]
        public string SimplePopupHeaderIconColor { get; set; } = "orangered";
        [Parameter]
        public bool SimplePopupCheckBox1 { get; set; } = true;
        [Parameter]
        public bool SimplePopupCheckBox2 { get; set; } = true;
        [Parameter]
        public bool SimplePopupCheckBox3 { get; set; } = true;
        [Parameter]
        public bool SimplePopupCheckBox1Visible { get; set; } = true;
        [Parameter]
        public bool SimplePopupCheckBox2Visible { get; set; } = true;
        [Parameter]
        public bool SimplePopupCheckBox3Visible { get; set; } = true;
    }
}
