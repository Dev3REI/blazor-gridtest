﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.IO;

using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace WebApplication1.Pages
{
    public class MyCrypt
    {
        private const string s1_name = "lQHN+lwFd4sUQZPuEkt0CWbsgqdCdZOQhOY6tElzsTbDB0MeaEqbEQwyen0nxoe/ARNKO8QyAYW99aCTlUtYRIC53X2/OLjd2cXi3sO1J13RCBBPS/ItCstK0woClvsdeNhZVRFP1LQGwJ9wBfbVj/eChWr5kMQDXhdtdGU+t4JDlyFLylwJdvDDZ1PwKwOJ1iCxk1y7j3YwkcsAmTy1Zmnm02awJw9iKaZs8eKEzSj41H1NyP63aeM2jXMlxyJjx/+31ImE4tDI+U9PmZpN4vJXE5aKRehj"; // Scott
        //Data Source=203.13.68.50;Initial Catalog=ezyrez;Persist Security Info=True;User ID=ezyrez;Password=35UPmQez
        //db.ezyrez.com.au new -->  lQHN+lwFd4sUQZPuEkt0CWbsgqdCdZOQO4IWsNSovhhnZcvSyLh0L3ehc0c17dAtitprljvqjwry9NJhvyi8i5vVT+2YRIVJSYO4uOqSSivhyfH6Fft3rBakBKloL7iCCO8kI5ud726zwqOpAocpLL2lLRBObBK9a/c+Oyi9c7v7XRJUrzosrQlgXn2j+WEpW2wpFo2asFR3ByNtBpQ9prftV+Etycn9TCaF30aqUPnxRiBbci5vsI5i2A1JDy3k9FslAs2wpqF+YTk+RpnV2ifHjxzoXGb7JOeJWPLafa4=
       
        private const string s1_name2 = "MH+8lbGrGYYb5qFFnxICq+21P1OcoAAKPTO9RW7oYNQOM/HlmqF/Z2zdH3wIP7klJLtbthEHuvnttT9TnKAACumU5z1HjNsfva5Uzq8P8ibiuMDi9ekOb6uQUuzUHijftqC38zyRT9Bp9weUUMchLPaLfQAwwtHb6ozP601qyONpAWjqM2ULp7Fpe5gXB2QbbWoyb7DLCy3ekNHryMwTW6jxNePhKMXlHuyAnRv1yjyZhFQQlIlYaWJiekmsaQfYFC2jxNqKXwAvwJVLcjgB/rvXW9R24onDummZWOKRjzl0xGluAxFx1dv+Vcme8ykRh4fx2l+C5PitGLMP4yUxS90xIlCkBQzc";
        //metadata=res://*/ezyrez.csdl|res://*/ezyrez.ssdl|res://*/ezyrez.msl;provider=System.Data.SqlClient;provider connection string='data source=203.13.68.50;initial catalog=ezyrez;user id=ezyrez;password=35UPmQez;MultipleActiveResultSets=True;App=EntityFramework'
        //new --> MH+8lbGrGYYb5qFFnxICq+21P1OcoAAKPTO9RW7oYNQOM/HlmqF/Z2zdH3wIP7klJLtbthEHuvnttT9TnKAACumU5z1HjNsfva5Uzq8P8ibiuMDi9ekOb6uQUuzUHijftqC38zyRT9Bp9weUUMchLPaLfQAwwtHb6ozP601qyONpAWjqM2ULp11bMvhUDWMSZmiRjJ+dtaR5gA+H9bCQaHmhxsnMjETjWxnV3cNkXbiepdQZ6FMXwpLnnFCXKMDIRi3Mlj7kCq+wMWZnmfTnXynSDM5yfDQI1nuVcOLgn7+u31MKbLrFzzzEZCgfip75y/G/X4nLBjlMBU7ucE7CVONydP/N0oS+

        public static string EzyConnStr()
        {   
            var conn = deCryptIt1(s1_name);  //s1_name
            //conn = Decrypt("s1_name2"); //s1_name2
            conn = "Server=tcp:reimaster.database.windows.net,1433;Initial Catalog=TurtleBeachRei;Persist Security Info=False;User ID=reimaster;Password=rei#master!16;MultipleActiveResultSets=False; Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            //conn = "Data Source=sbs2011.database.windows.net,1433;Initial Catalog=TurtleBeachRei;Persist Security Info=True;User ID=reiadmin;Password=D0n0770uc#;Connection Timeout = 0";
            //conn = "Data Source=sql\\reisql12;Initial Catalog=turtlebeachrei;Persist Security Info=True;User ID=sa;Password=Savvy123";
           // conn = "Data Source=sql\\reisql12;Initial Catalog=reionline;Persist Security Info=True;User ID=sa;Password=Savvy123";

            return conn;
        }

        static string pwordEzy = "@#1Qd34Tux^1";
        public static string deCryptIt1(string cipherText)
        {
            Simple3Des wrapper = new Simple3Des(pwordEzy);
            // DecryptData throws if the wrong password is used.
            try
            {
                string plainText = wrapper.DecryptData(cipherText);
                return plainText;

            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {
                var ww = ex.Message;
                return "The data could not be decrypted with the password.";
            }
        }

        public static string enCryptIt1(string cipherText)
        {
            Simple3Des wrapper = new Simple3Des(pwordEzy);
            // DecryptData throws if the wrong password is used.
            try
            {
                string plainText = wrapper.EncryptData(cipherText);
                return plainText;

            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {
                var ww = ex.Message;
                return "The data could not be encrypted with the password.";
            }
        }

        private static string Encrypt(string data)
        {
            //data = "ramework'";
            TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5Provider = new MD5CryptoServiceProvider();

            DES.Mode = CipherMode.ECB;
            DES.Key = hashMD5Provider.ComputeHash(Encoding.UTF8.GetBytes("Yes$r3Wcrx0Bvcf"));

            DES.Padding = PaddingMode.PKCS7;
            ICryptoTransform DESEncrypt = DES.CreateEncryptor();
            Byte[] Buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(data);

            return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }
        private static string Decrypt(string data)
        {
            TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5Provider = new MD5CryptoServiceProvider();
            DES.Mode = CipherMode.ECB;
            DES.Key = hashMD5Provider.ComputeHash(Encoding.UTF8.GetBytes("Yes$r3Wcrx0Bvcf"));

            DES.Padding = PaddingMode.PKCS7;
            ICryptoTransform DESEncrypt = DES.CreateDecryptor();
            Byte[] Buffer = Convert.FromBase64String(data.Replace(" ", "+"));

            return Encoding.UTF8.GetString(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

        public sealed class Simple3Des
        {
            private TripleDESCryptoServiceProvider TripleDes = new TripleDESCryptoServiceProvider();
            /// <summary>
            /// Create the hash for the encrption
            /// </summary>
            /// <param name="key"></param>
            /// <param name="length"></param>
            /// <returns></returns>
            /// <remarks></remarks>
            private byte[] TruncateHash(string key, int length)
            {
                SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();

                // Hash the key.
                byte[] keyBytes = System.Text.Encoding.Unicode.GetBytes(key);
                byte[] hash = sha1.ComputeHash(keyBytes);

                // Truncate or pad the hash.
                Array.Resize(ref hash, length);
                return hash;
            }

            public Simple3Des(string key)
            {
                // Initialize the crypto provider.
                TripleDes.Key = TruncateHash(key, TripleDes.KeySize / 8);
                TripleDes.IV = TruncateHash("", TripleDes.BlockSize / 8);
            }

            /// <summary>
            /// Encrypt data
            /// </summary>
            /// <param name="plaintext">The text to encrypt</param>
            /// <returns></returns>
            /// <remarks></remarks>
            public string EncryptData(string plaintext)
            {
                // Convert the plaintext string to a byte array.
                byte[] plaintextBytes = System.Text.Encoding.Unicode.GetBytes(plaintext);

                // Create the stream.
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                // Create the encoder to write to the stream.
                CryptoStream encStream = new CryptoStream(ms, TripleDes.CreateEncryptor(), System.Security.Cryptography.CryptoStreamMode.Write);

                // Use the crypto stream to write the byte array to the stream.
                encStream.Write(plaintextBytes, 0, plaintextBytes.Length);
                encStream.FlushFinalBlock();

                // Convert the encrypted stream to a printable string.
                return Convert.ToBase64String(ms.ToArray());
            }

            /// <summary>
            /// Encrypt data
            /// </summary>
            /// <param name="encryptedtext">The text to decrypt</param>
            /// <returns></returns>
            /// <remarks></remarks>
            public string DecryptData(string encryptedtext)
            {
                // Convert the encrypted text string to a byte array.
                byte[] encryptedBytes = Convert.FromBase64String(encryptedtext);

                // Create the stream.
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                // Create the decoder to write to the stream.
                CryptoStream decStream = new CryptoStream(ms, TripleDes.CreateDecryptor(), System.Security.Cryptography.CryptoStreamMode.Write);

                // Use the crypto stream to write the byte array to the stream.
                decStream.Write(encryptedBytes, 0, encryptedBytes.Length);
                decStream.FlushFinalBlock();

                // Convert the plaintext stream to a string.
                return System.Text.Encoding.Unicode.GetString(ms.ToArray());
            }

        }
    }
}