﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Pages
{
    public class CounterBase : ComponentBase
    {
        [Parameter]
        public int IncrementAmt { get; set; } = 2;

        public int currentCount = 0;

        public void IncreaseCount1()
        {
            currentCount = currentCount + IncrementAmt;
        }
    }
}
