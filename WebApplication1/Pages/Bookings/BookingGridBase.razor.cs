﻿
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Microsoft.VisualBasic;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Navigations;
using Syncfusion.Blazor.Popups;
using Syncfusion.Blazor.Schedule;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApplication1.Pages.BookingServices;

namespace WebApplication1.Pages.Bookings
{
    public class BookingGridBase : ComponentBase
    {
        public string testSQLStr = "";
        [Inject]
        IJSRuntime JSRuntime { get; set; }
        [Inject]
        public HttpClient aHttpClient { get; set; }

        [Inject]
        protected Blazored.LocalStorage.ILocalStorageService localStore { get; set; }
        //[Inject]
        //protected  BookingServices.BService mybookingService { get; set; } // static type does not work with DI
        [Inject]
        public BookingServices.BookingModuleData pBookingModuleData { get; set; }
        [Inject]
        BrowserService aBrowserService { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Parameter]
        public BookingParameter myPara { get; set; }

        public static string gstrBookingStockDesc = "Room";
        public static string glngUserID = "4";
        public static string a_connEncEzy = "";// MyCrypt.EzyConnStr();
        public static string nl = System.Environment.NewLine;
        public static string pGridPeriodText { get; set; } = "Months"; // or Days
        public static int? pGridPeriodValues { get; set; } = 2; // default value -> 2 Months;  //  or aDays 60 (about 9 weeks)
        public static int pGridPeriodMax { get; set; } = 13; // or 400
        public static int pGridPeriodMin { get; set; } = 1;  // or 10
        public static int pGridPeriodStep { get; set; } = 1; // or 5
        public static string gstrVersion = "";  //Z for readonly
        public static string gLoggedInDateTime = "";  //
        public static string[] Resources { get; set; } = { "RoomTypes", "RoomCodes" };
        public static string[] ResourcesRoomonly { get; set; } = { "RoomCodes" };

        ///////////////////////////////////////
        #region grid settings 
        public class RoomTypeDropdownData { public string ID { get; set; } public string Text { get; set; } }
        public class BookingTypeDropdownData { public string ID { get; set; } public string Text { get; set; } }
        public class AgentDropdownData { public string ID { get; set; } public string Text { get; set; } }

        public BookingUserSettings pBookingSetting; // also sets pBookingGridDisplaySettings
                                                                                           // below has grid sort settings -  /1 roomtype, 0 roomcode -- grid sort settings 
                                                                                           //pBookingGridDisplaySettings.gBookingGrid_Sort   /1 roomtype, 0 roomcode --
        public BookingGridDisplaySetting pBookingGridDisplaySettings; // do not move this up.. or do not make  new BookingGridDisplaySetting() here
        #endregion
        ///////////////////////////////////////
        ///
        public  List<RoomTypeAndRoomData> RoomTypeData; // this also has data for roomtype dropdown
        public  List<RoomTypeDropdownData> RoomTypeDropdownList; // =  new List<RoomTypeDropdownData>(); 
        public  List<BookingTypeDropdownData> BookingTypeDropdownList ;
        public  List<AgentDropdownData> AgentDropdownList;

        public List<RoomTypeAndRoomData> RoomCodeData;// = GenerateRoomCodeData();

        public  List<BookingData> DataSource; // = GenerateBookings1(DateTime.Today, pGridPeriodValues ?? 2);

        public string pRoomTypeIdSelected { get; set; } = "";

        // need below vaues to shwow  showing thse dates text
        public static DateTime pMinDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);   //DateTime.Today;
        public static DateTime pMaxDate = pMinDate.AddMonths(pGridPeriodValues ?? 2).AddDays(-1);
        public static long pMonthInterval = 2; // value on the grid month interval  // DO NOT MOVE

        public static List<WeekOfYear> WeekOfYearData = GenerateWeekOfYear(DateTime.Today, pGridPeriodValues ?? 2);
        public static string WeekOfYearText { get; set; } = "";


        public static string pShowing = $"From {pMinDate.ToString("dd/MM/yy")} To {pMaxDate.ToString("dd/MM/yy")}";  //
        public DateTime gridSelectedDate { get; set; } = DateTime.Today; // this is "selected date" on the grid control
        public DateTime? myDatePickerDate { get; set; } = DateTime.Today; // this is datetimepicker date

        public SfSchedule<BookingData> ScheduleObj;
        public SfMultiSelect<string[]> RoomTypeDropdownObj;
        public SfMultiSelect<string[]> BookingTypeDropdownObj;
        public SfMultiSelect<string[]> AgentTypeDropdownObj;
        public static SfDropDownList<string, WeekOfYear> WeekOfYearDropdownObj;
        public SfMenu myMenu1;

        public static bool p_Show_AgentName = false;
        protected string pWaitIcon = "fa fa-refresh"; //"fa fa-refresh";  //"fa fa-spinner fa-spin"; 

        public SfMenu menuBookingTop;
        protected int pWindowHeight { get; set; } = 720;
        protected int pWindowWidth { get; set; } = 800;
        protected TimeZoneInfo pClientTimeZone { get; set; }

        protected string pRoomtypeColumnWidth { get; set; } = "250px";
        protected static string MnuBookingText = "Booking";
        protected static string MnuSearchText = "Search";
        protected static string MnuRoomSearchText = "Room Search";
        protected static string MnuRecentText = "Recent";
        protected static string MnuAllotmentsText = "Allotments";
        protected static string MnuHireText = "Hire";
        protected static string MnuProcessText = "Process";
        protected static string MnuReportsText = "Reports";
        protected static string MnuSettingsText = "Settings";

        protected string BookingChangeHeader { get; set; } = "REIMaster";
        protected string BookingChangeContent { get; set; } = "";
        protected string BookingEnquiryContent { get; set; } = "";
        protected string BookingGSTContent { get; set; } = "";
        protected string BookingChangeImage { get; set; } = "fa-warning";
        protected bool BookingChangeShowBottomCloseButton { get; set; } = false;
        protected bool BookingChangeIsBookingChangeMessage { get; set; } = true;
        protected string BookingChangeHeaderBackground { get; set; } = "cornflowerblue";
        protected string BookingChangeHeaderIconColor { get; set; } = "orangered";
        protected bool BookingChangeCheckBox1 { get; set; } = true;
        protected bool BookingChangeCheckBox2 { get; set; } = true;
        protected bool BookingChangeCheckBox3 { get; set; } = true;
        protected bool BookingChangeCheckBox1Visible { get; set; } = true;
        protected bool BookingChangeCheckBox2Visible { get; set; } = true;
        protected bool BookingChangeCheckBox3Visible { get; set; } = true;
        protected BookingData BookingChangeBookingItem { get; set; }

        public bool pShowChangeTariffMessage { get; set; } = false;
        public string p_MoveDirection { get; set; } = "";
        public bool pIsRoomTypeChange { get; set; } = false;
        public bool BookingChangeDialogVisible { get; set; } = false;
        public bool BookingChangeDialogHeaderCloseButtonShow { get; set; } = false;
        protected bool isLoading;

        //for dual key popup message
        public bool DialogDualKeyVisible { get; set; } = false;
        public string DualKeyPopupContentDualKey { get; set; } = "";
        public bool DualKeyPopupShow3Buttons { get; set; } = false;
        public string SimplePopupDualKeyMoveCase { get; set; } = "";
        public bool p_To_DualKey { get; set; } = false;
        public BookingData SimplePopupBookingDualKey { get; set; }

        //public static string aStatic = "";
        //protected   override void OnInitialized()
        //{
        //    try
        //    {
        //       var se12 = 33; // aAppData.Age;
        //        se12 = 0 + se12;
        //    }
        //    catch (Exception aa)
        //    {
        //        var qq = aa.Message;
        //        //  throw;
        //    }
        //    //aStatic = bookingSettings.ServiceId.ToString();

        //    //  var ss = await GenerateWeekOfYear();  // await ...
        //}
        public void Dispose()
        {
            try
            {
                var aAge = pBookingModuleData.Age;
                p_Show_AgentName = false;
                //if (aAge < 3)
                //{
                //    RoomTypeData.Clear();
                //    NavigationManager.NavigateTo("booking/index", true);
                //}

            }
            catch (Exception aa)
            {
                var qq = aa.Message;
                //  throw;
            }
        }
        protected override void OnParametersSet()
        {
            try
            {
                var a_now = DateTime.Now.ToString();
                //pBookingModuleData.OpenWindowNames.Clear();
                //pBookingModuleData.OpenWindowNames.Add(a_now);
                gLoggedInDateTime = myPara.WidowOpenDateTime;
                a_connEncEzy = MyCrypt.EzyConnStr();
                pBookingSetting = GenerateBookingUserSettings();
                RoomTypeData = GenerateRoomTypeData();
                BookingTypeDropdownList = GenerateBookingType();
                RoomCodeData = GenerateRoomCodeData();
                DataSource = GenerateBookings1(DateTime.Today, pGridPeriodValues ?? 2);

                pWindowHeight = myPara.WindowHeight - 100;
                pWindowWidth = myPara.WindowWidth;
                pClientTimeZone = GetTimeZoneInfo(myPara.ClientTimeZoneName);
                if (pWindowWidth < 600)
                {
                    pRoomtypeColumnWidth = "100px";
                    MnuBookingText = "Booking";
                    MnuSearchText = "";
                    MnuRoomSearchText = "";

                    MnuRecentText = "";
                    MnuAllotmentsText = "";
                    MnuHireText = "";
                    MnuProcessText = "";

                    MnuReportsText = "";
                    MnuSettingsText = "";
                }
                else
                {
                    pRoomtypeColumnWidth = "250px";
                    MnuBookingText = "Booking";
                    MnuSearchText = "Search";
                    MnuRoomSearchText = "Room Search";

                    MnuRecentText = "Recent";
                    MnuAllotmentsText = "Allotment";
                    MnuHireText = "Hire";
                    MnuProcessText = "Process";

                    MnuReportsText = "Reports";
                    MnuSettingsText = "Settings";

                }

                base.OnParametersSet();
            }
            catch (Exception aa)
            {
                var qq = aa.Message;
                //  throw;
            }

        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            try
            {
                if (firstRender)
                {
                   
                    // because i am using server side blazor i need to add this call(localstorage) to onafterrender async 
                    // if it is client side, you can put it in initialise function
                    await localStore.SetItemAsync("gUseDescOnBooking", "John Smith");
                    //  var name = await localStore.GetItemAsync<BookingSidePanelLocalData>("gBookingSidePanelLocalData");
                    //  var rty = name.RecentBookingList[0].GuestName + "";
                    // await ScheduleObj.ScrollToResource(975, "RoomCodes"); // 1133 -  1033 room code
                    //this.StateHasChanged();
                    //   var dimension = await aBrowserService.GetDimensions();
                    //   pWindowWidth = dimension.Width;
                    //   pWindowHeight = dimension.Height;
                    //   if (pWindowWidth < 8000)
                    //   {
                    //        var ww = (List<MenuItem>)myMenu1.Items;
                    //        ww[0].Text = "";
                    //  }
                }
                else
                {
                    // var dimension = await aBrowserService.GetDimensions();
                    //  pWindowWidth = dimension.Width;
                    // pWindowHeight = dimension.Height - 100;
                    //    StateHasChanged();
                    // await Task.Run(() => WeekOfYearData = GenerateWeekOfYear(myDatePickerDate, pGridPeriodValues ?? 2));
                    // await ScheduleObj.ScrollToResource(975, "RoomCodes"); // 1133 -  1033 room code
                    var sq = 2;
                    sq = sq + 3;
                }
                // await ScheduleObj.ScrollToResource(975, "RoomCodes");
                //var aAge = aAppData.Age;
                //if (aAge < 3)
                //{
                //    RoomTypeData.Clear();
                //    NavigationManager.NavigateTo("booking/index", true);
                //}
            }
            catch (Exception aa)
            {
                var qq = aa.Message;
                //  throw;
            }
        }

        public void GoToday()
        {
            try
            {
                // pWaitIcon = "fa fa-spinner fa-spin";
                myDatePickerDate = DateTime.Today;
                RefreshBookingGrid();
                //  pWaitIcon = "fa fa-refresh";
            }
            catch (Exception cc)
            {
                var cc1 = cc.Message;
                // throw;
            }
        }

        public async void RefreshBookingGrid()
        {
            try
            {
                if (isLoading)
                {
                    return;
                }

                isLoading = true;
                // pWaitIcon = "fa fa-spinner fa-spin";
                //try
                //{
                //    var dimension = await aBrowserService.GetDimensions();
                //    pWindowHeight = dimension.Height - 100;//pWindowWidth = dimension.Width;
                //}
                //catch (Exception cc1)
                //{
                //    var cc556 = cc1.Message;
                //}


                var ss = 3;
                var roomTypesSelected = "";
                var bookingTypeSelected = "";
                var agentTypeSelected = "";

                if (RoomTypeDropdownObj.Value != null && RoomTypeDropdownObj.Value.Length > 0)
                {
                    roomTypesSelected = string.Join(",", RoomTypeDropdownObj.Value);
                }
                if (BookingTypeDropdownObj != null)
                {
                    if (BookingTypeDropdownObj.Value != null && BookingTypeDropdownObj.Value.Length > 0)
                    {
                        bookingTypeSelected = BookingTypeDropdownObj.Value[0];
                    }
                    if (AgentTypeDropdownObj.Value != null && AgentTypeDropdownObj.Value.Length > 0)
                    {
                        agentTypeSelected = string.Join(",", AgentTypeDropdownObj.Value);
                    }
                }

                if (pBookingGridDisplaySettings.gBookingGrid_Sort == "1")
                {
                    RoomTypeData = GenerateRoomTypeData(roomTypesSelected);
                }

                //await Task.Run(() => RoomCodeData = GenerateRoomCodeData(roomTypesSelected));
                //await Task.Run(() => DataSource = GenerateBookings1(myDatePickerDate, pGridPeriodValues ?? 2, roomTypesSelected, bookingTypeSelected, agentTypeSelected));
                RoomCodeData = GenerateRoomCodeData(roomTypesSelected);
                DataSource = GenerateBookings1(myDatePickerDate, pGridPeriodValues ?? 2, roomTypesSelected, bookingTypeSelected, agentTypeSelected);
                WeekOfYearData = GenerateWeekOfYear(myDatePickerDate, pGridPeriodValues ?? 2);
                //await Task.Run(() => WeekOfYearData = GenerateWeekOfYear(myDatePickerDate, pGridPeriodValues ?? 2));
                gridSelectedDate = myDatePickerDate ?? DateTime.Today;

                WeekOfYearText = "";
                ss = pGridPeriodValues ?? 0 + ss;
                await Task.Delay(3);
                // ScheduleObj.Refresh();
                // var dimension = await aBrowserService.GetDimensions();
                isLoading = false;

                //  this.StateHasChanged();

                //ScheduleObj.SelectedDate =DateTime.Today
                //pWaitIcon = "fa fa-refresh";

            }
            catch (Exception hh)
            {
                // pWaitIcon = "fa fa-refresh";
                isLoading = false;
                var qq = hh.Message;
                //throw;
            }
        }

         public BookingUserSettings GenerateBookingUserSettings()
        {
            try
            {
                // i do not want to make several connections in case we move to balzor client side, so use one connection and
                // get several data
                // a_connEncEzy = "Data Source=sql\\reisql12;Initial Catalog=turtlebeachrei;Persist Security Info=True;User ID=sa;Password=Savvy123";
                DataTable dt_defaults = new DataTable();
                DataTable dt_gridDisplay = new DataTable();
                DataTable dt_agent = new DataTable();

                var rowFound = 0;
                if (a_connEncEzy != "")
                {
                    using (SqlConnection conn = new SqlConnection(a_connEncEzy))
                    {
                        //////////////////////
                        //grid setting - general
                        conn.Open();
                        var aSQL = ""; // 
                        aSQL = " SELECT BookingStockDescription,isnull(DisplayDescriptionOnBookingGrid,0) DisplayDescriptionOnBookingGrid,isnull(UseNettAccommMethod,0) UseNettAccommMethod FROM TBL_Defaults WHERE ID = 1 ";

                        using (SqlDataAdapter adapter1 = new SqlDataAdapter(aSQL, conn))
                        {
                            adapter1.Fill(dt_defaults);
                            rowFound = dt_defaults.Rows.Count;
                            var ss1 = 3;
                            /////////////////////////
                            // grid user settings
                            aSQL = "select BookingGridRoomColumnWidth,BookingGridRowHeight,BookingGridColumnWidth, ";
                            aSQL += " BookingGridStyle,BookingGridRoomSortSequence,BookingGridRoomFilterType,BookingGridRoomFilterName from tbl_useroptions where userid = " + glngUserID;

                            adapter1.SelectCommand.CommandText = aSQL;
                            adapter1.Fill(dt_gridDisplay);
                            //////////////////////////
                            /////agent
                            aSQL = "SELECT ID, Reference from TBL_Creditors   ";
                            aSQL += "where Inactive = 0 and Agent = 1 AND DoNotShowAllotments = 0 order by reference ";

                            adapter1.SelectCommand.CommandText = aSQL;
                            adapter1.Fill(dt_agent);
                            ss1 = ss1 + 4;
                        }
                        pBookingGridDisplaySettings = new BookingGridDisplaySetting();
                        pBookingGridDisplaySettings.gBookingRoomCode_Width = dt_gridDisplay.Rows.Count > 0 ? dt_gridDisplay.Rows[0]["BookingGridRoomColumnWidth"].ToString() : "90";
                        pBookingGridDisplaySettings.gBookingGrid_Sort = dt_gridDisplay.Rows.Count > 0 ? dt_gridDisplay.Rows[0]["BookingGridRoomSortSequence"].ToString() : "1"; // default to roomtype
                        var aGridFilterName = (dt_gridDisplay.Rows[0]["BookingGridRoomFilterName"]??"").ToString();
                        pBookingGridDisplaySettings.pBookingGrid_TimePeriodBy = "Months"; //Months or Days  -  default
                        if (aGridFilterName.Contains(";"))
                        {
                            var a_filterArray = aGridFilterName.Split(';');
                            pBookingGridDisplaySettings.pBookingGrid_FilterName1 = a_filterArray[0];
                            pBookingGridDisplaySettings.pBookingGrid_FilterName2 = a_filterArray[1];
                            //etc
                            if (a_filterArray.Length == 6)
                            { }
                            if (a_filterArray.Length == 7)
                            { pBookingGridDisplaySettings.pBookingGrid_ShowDualkeyLink = a_filterArray[6]; }
                            if (a_filterArray.Length == 8)
                            {
                                pBookingGridDisplaySettings.pBookingGrid_TimePeriodBy = a_filterArray[7]; //month or day
                            }
                        }
                        else
                        {
                            pBookingGridDisplaySettings.pBookingGrid_FilterName1 = "0";
                            pBookingGridDisplaySettings.pBookingGrid_FilterName2 = "0";
                            pBookingGridDisplaySettings.pBookingGrid_FilterBookingType = "0";
                            pBookingGridDisplaySettings.pBookingGrid_FilterAgent = "0";
                            pBookingGridDisplaySettings.pBookingGrid_ShowDualkeyLink = "0";
                            pBookingGridDisplaySettings.pBookingGrid_TimePeriodBy = "Months"; //Months or Days
                        }
                        SetGridPeriodInterval(pBookingGridDisplaySettings.pBookingGrid_TimePeriodBy);
                        //gBookingGrid_FilterName = pBookingGrid_FilterName1 & ";" & pBookingGrid_FilterName2 & ";" & pBookingGrid_FilterBookingType & ";" & pBookingGrid_FilterAgent
                        //gBookingGrid_FilterName &= ";" & pBookingGrid_CollpaseRows & ";" & pBookingGrid_ShowAgent & ";" & pBookingGrid_ShowDualkeyLink
                        // need to add pBookingGrid_TimePeriodBy at the end
                        //QExec("update tbl_useroptions  set BookingGridRoomFilterName  = '" & gBookingGrid_FilterName & "' where UserID = " & glngUserID)
                        /////////////// 
                        ///agent
                        var agentList = new List<AgentDropdownData>();

                        for (int i = 0; i < dt_agent.Rows.Count; i++)
                        {
                            agentList.Add(new AgentDropdownData { ID = dt_agent.Rows[i]["ID"].ToString(), Text = dt_agent.Rows[i]["Reference"].ToString() });
                        }
                        AgentDropdownList = agentList;

                    }
                }
                return new BookingUserSettings()
                {
                    gstrBookingStockDesc = rowFound > 0 ? dt_defaults.Rows[0]["BookingStockDescription"].ToString() : "", //"Room"
                    gUseDescOnBooking = rowFound > 0 ? (bool)(dt_defaults.Rows[0]["DisplayDescriptionOnBookingGrid"] ?? false) : false
                };
            }
            catch (Exception aa)
            {
                var qq = aa.Message;
                return new BookingUserSettings();
            }
        }
        // roomtype gets the roomtype dropdwon data also (e.g. roomtype filter combo box)
         public List<RoomTypeAndRoomData> GenerateRoomTypeData(string room_types = "")
        {
            var roomTypeList = new List<RoomTypeAndRoomData>();
            if (a_connEncEzy == "")
            {
                return roomTypeList;
            }
            try
            {
                List<string> roomtypeFilter = room_types.Split(',').ToList();

                //a_connEncEzy = "Data Source=203.13.68.50;Initial Catalog=ezyrez;Persist Security Info=True;User ID=ezyrez;Password=35UPmQez";
                //a_connEncEzy = "Data Source=sql\\reisql12;Initial Catalog=turtlebeachrei;Persist Security Info=True;User ID=sa;Password=Savvy123";
                using (SqlConnection conn = new SqlConnection(a_connEncEzy))
                {
                    //////////////////////
                    conn.Open();
                    var aSQL = ""; // TOP (80) <<<<<<< -*****************************
                    aSQL = " SELECT distinct P.reference, P.id,P.roomtypeid, T.GridColour, T.reference as roomTypename   " + nl;
                    if (pBookingSetting.gUseDescOnBooking)
                    {
                        aSQL = " SELECT distinct P.reference, P.id,P.roomtypeid, T.GridColour, T.Description as roomTypename " + nl;
                    }
                    aSQL += "  from tbl_property as P inner join tbl_roomTypes  as T on P.roomtypeid = T.id  " + nl;
                    aSQL += "  where P.Inactive = 0 and P.LettingType ='Holiday' and P.DoNotShowInBookingGrid = 0 " + nl;
                    if (pBookingSetting.gUseDescOnBooking)
                    {
                        aSQL += "   order by T.Description, P.reference " + nl;
                    }
                    else
                    {
                        aSQL += "   order by T.reference, P.reference " + nl;
                    }

                    var roomTypeNameStr = new List<string>();
                    if (RoomTypeDropdownList == null)
                    {
                        RoomTypeDropdownList = new List<RoomTypeDropdownData>();
                    }
                    RoomTypeDropdownList.Clear();
                    using (SqlDataAdapter adapter1 = new SqlDataAdapter(aSQL, conn))
                    {
                        DataTable dt_allRooms = new DataTable();
                        adapter1.Fill(dt_allRooms);
                        var rowFound = dt_allRooms.Rows.Count;

                        if (rowFound > 0)
                        {
                            var random = new Random();
                            for (int i = 0; i < rowFound; i++)
                            {
                                var aRoomTypeName = dt_allRooms.Rows[i]["roomTypename"].ToString();
                                var aRoomTypeId = int.Parse(dt_allRooms.Rows[i]["roomtypeid"].ToString());
                                if (!roomTypeNameStr.Contains(aRoomTypeName))
                                {
                                    roomTypeNameStr.Add(aRoomTypeName);
                                    var aExpand = true;
                                    if (i == 0)
                                    {
                                        aExpand = true;
                                    }
                                    var aRoomtype = new RoomTypeAndRoomData { RoomTypeText = aRoomTypeName, Id = aRoomTypeId, RoomTypeColor = String.Format("#{0:X6}", random.Next(0x1000000)), Expand = aExpand };

                                    if (room_types != "")
                                    {
                                        if (roomtypeFilter.Contains(aRoomTypeId.ToString()))
                                        {
                                            roomTypeList.Add(aRoomtype);
                                        }
                                    }
                                    else
                                    {
                                        roomTypeList.Add(aRoomtype);
                                    }

                                    RoomTypeDropdownList.Add(new RoomTypeDropdownData { ID = aRoomTypeId.ToString(), Text = aRoomTypeName });
                                }
                            }
                            var ss1 = 3;
                            ss1 = ss1 + 4;
                        }
                    }
                }

                //if ((pBookingGridDisplaySettings.gBookingGrid_Sort ?? "2") == "0") // means roomcode listing, room type listing -> 1
                //{
                //    roomTypeList.Add(new RoomTypeAndRoomData { RoomTypeText = "RoomCode", Id = 1, RoomTypeColor = "#cb6bb2" });
                //}

                return roomTypeList; // resources;
            }
            catch (Exception aa)
            {
                var qq = aa.Message;
                return roomTypeList;
            }
        }
         public List<RoomTypeAndRoomData> GenerateRoomCodeData(string room_types = "")
        {
            var roomCodeList = new List<RoomTypeAndRoomData>();
            if (a_connEncEzy == "")
            {
                return roomCodeList;
            }
            try
            {
                using (SqlConnection conn = new SqlConnection(a_connEncEzy))
                {
                    //////////////////////
                    // STEP 1 get manager booking arrival emails
                    conn.Open();
                    var aSQL = ""; // TOP (80) <<<<<<< -*****************************
                    aSQL = " SELECT distinct P.reference, P.id,P.roomtypeid, T.GridColour, T.reference as roomTypename, isnull(InverseGridColour,0) as Inverse, isnull(OverrideGST,0) as GST1, isnull(GSTOnAccomm,0) as isGST, isnull(P.RoomIsBookable,0) as onlineBookable " + nl;
                    if (pBookingSetting.gUseDescOnBooking)
                    {
                        aSQL = " SELECT distinct P.reference, P.id,P.roomtypeid, T.GridColour, T.Description as roomTypename, isnull(InverseGridColour,0) as Inverse, isnull(OverrideGST,0) as GST1, isnull(GSTOnAccomm,0) as isGST, isnull(P.RoomIsBookable,0) as onlineBookable " + nl;
                    }
                    aSQL += " , isnull(P.DualKeyRoom,0) as isDualKey, isnull(P.DualKeyLinkedRoomID,0) as DualKeyLinkedRoomID, isnull(P.DualKeyLinkedRoomTypeID,0) as DualKeyResultRoomTypeID, isnull((select p1.reference from tbl_property as P1 where p1.id = p.DualKeyLinkedRoomID),'') as DualKeyLinkedRoomCodeName " + nl;

                    aSQL += "  from tbl_property as P inner join tbl_roomTypes  as T on P.roomtypeid = T.id  " + nl;
                    aSQL += "  where P.Inactive = 0 and P.LettingType ='Holiday' and P.DoNotShowInBookingGrid = 0 " + nl;
                    if (room_types != "")
                    {
                        aSQL += " and P.roomtypeid in (" + room_types + ") " + nl;
                    }
                    if ((pBookingGridDisplaySettings.gBookingGrid_Sort ?? "2") == "0")
                    {
                        aSQL += " order by P.reference  " + nl;
                    }
                    else
                    {
                        if (pBookingSetting.gUseDescOnBooking)
                        {
                            aSQL += "   order by T.Description, P.reference " + nl;
                        }
                        else
                        {
                            aSQL += "   order by T.reference, P.reference " + nl;
                        }
                    }
                    var roomCodeNameStr = new List<string>();

                    using (SqlDataAdapter adapter1 = new SqlDataAdapter(aSQL, conn))
                    {
                        DataTable dt_allRooms = new DataTable();
                        adapter1.Fill(dt_allRooms);
                        var rowFound = dt_allRooms.Rows.Count;

                        if (rowFound > 0)
                        {
                            var random = new Random();
                            for (int i = 0; i < rowFound; i++)
                            {
                                var aRoomCodeName = dt_allRooms.Rows[i]["reference"].ToString();
                                var aRoomCodeId = int.Parse(dt_allRooms.Rows[i]["id"].ToString());
                                var aRoomTypeId = int.Parse(dt_allRooms.Rows[i]["roomtypeid"].ToString());
                                var aRoomGSTInfo = dt_allRooms.Rows[i]["GST1"].ToString() + dt_allRooms.Rows[i]["isGST"].ToString();
                                var aRoomOnlineBookable = bool.Parse(dt_allRooms.Rows[i]["onlineBookable"].ToString());

                                var aDualKey = bool.Parse(dt_allRooms.Rows[i]["isDualKey"].ToString());
                                var aDualKeyLinkedRoomID = int.Parse(dt_allRooms.Rows[i]["DualKeyLinkedRoomID"].ToString());
                                var aDualKeyResultRoomTypeID = int.Parse(dt_allRooms.Rows[i]["DualKeyResultRoomTypeID"].ToString());
                                var aDualKeyLinkedRoomCodeName = dt_allRooms.Rows[i]["DualKeyLinkedRoomCodeName"].ToString();
                                var aRoomCodeColor = dt_allRooms.Rows[i]["GridColour"].ToString(); //ConvertToHex(
                                var aForeColor = bool.Parse(dt_allRooms.Rows[i]["Inverse"].ToString()) ? "white" : "black";
                                //if ((pBookingGridDisplaySettings.gBookingGrid_Sort ?? "2") == "0")
                                //{
                                //    aRoomTypeId = 1;
                                //}
                                if (!roomCodeNameStr.Contains(aRoomCodeName))
                                {
                                    roomCodeNameStr.Add(aRoomCodeName);
                                    var aRoomCode = new RoomTypeAndRoomData
                                    {
                                        RoomCodeText = aRoomCodeName,
                                        Id = aRoomCodeId,
                                        RoomTypeGroupId = aRoomTypeId,
                                        RoomListRoomType_Id = aRoomTypeId,
                                        RoomTypeColor = String.Format("#{0:X6}", random.Next(0x1000000)),
                                        RoomCodeColor = ConvertToHex(aRoomCodeColor),
                                        DualKeyLinkedRoomCodeName = aDualKeyLinkedRoomCodeName,
                                        RoomCodeForeColor = aForeColor
                                    };
                                    roomCodeList.Add(aRoomCode);
                                }
                            }

                            var ss1 = 3;
                            ss1 = ss1 + 4;
                        }
                    }
                }
                return roomCodeList; // resources;
            }
            catch (Exception aa)
            {
                var qq = aa.Message;
                return roomCodeList;
            }
        }

        public static List<BookingTypeDropdownData> GenerateBookingType()
        {
            try
            {
                var aBookingTypeList = new List<BookingTypeDropdownData>();
                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_Bookings.Status In (2,3)", Text = "Bookings / In House" });
                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_Bookings.Status = 2", Text = "Bookings" });
                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_Bookings.Status = 3", Text = "In House" });
                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_Bookings.Status = 4", Text = "Departed" });
                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_Bookings.Status = 2  and ArrivalDateTime <= '" + DateTime.Today.ToString("dd MMM yyyy") + "'", Text = "Overdue Arrivals" });
                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_Bookings.Status = 3 and DepartureDateTime <= '" + DateTime.Today.ToString("dd MMM yyyy") + "'", Text = "Overdue Departures" });

                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_Bookings.Status = 2 and convert(nvarchar(8), ArrivalDateTime, 112) = '" + DateTime.Today.ToString("yyyyMMdd") + "'", Text = "Today's Arrivals" });
                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_Bookings.Status in( 2,3) and convert(nvarchar(8),DepartureDateTime,112) = '" + DateTime.Today.ToString("yyyyMMdd") + "'", Text = "Today's Departures" });
                aBookingTypeList.Add(new BookingTypeDropdownData { ID = " AND tbl_contacts.Blacklist = 1", Text = "Blacklist" });

                return aBookingTypeList;
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
                return new List<BookingTypeDropdownData>();
                // throw;
            }
        }

        public static void ShowWeekOfYearText()
        {
            try
            {
                //var a_date = DateTime.Today;
                var a_weekNo = int.Parse(Strings.Left(WeekOfYearDropdownObj.Text, 2)) - 1;
                var a_year = int.Parse(Strings.Right(WeekOfYearDropdownObj.Text, 4));
                //a_date = DateAndTime.DateAdd(DateInterval.Day, -2, DateAndTime.DateAdd(DateInterval.WeekOfYear, a_weekNo, new DateTime(a_year, 1,1)));

                DateTime jan1 = new DateTime(a_year, 1, 1);
                int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
                DateTime firstMonday = jan1.AddDays(daysOffset);

                var cal = CultureInfo.CurrentCulture.Calendar;
                int firstWeek = cal.GetWeekOfYear(firstMonday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

                if (firstWeek <= 1)
                {
                    a_weekNo -= 1;
                }

                DateTime result = firstMonday.AddDays(a_weekNo * 7);

                // todayDate
                WeekOfYearText = result.ToString("d/M");

            }
            catch (Exception aa)
            {
                var qq = aa.Message;
                WeekOfYearText = "";
                //throw;
            }
        }
        public static List<WeekOfYear> GenerateWeekOfYear(DateTime? fromDate, int aPeriodValues)
        {
            try
            {
                if (pGridPeriodText == "Months")
                {
                    pMinDate = new DateTime((fromDate ?? DateTime.Today).Year, (fromDate ?? DateTime.Today).Month, 1);
                    pMaxDate = pMinDate.AddMonths(aPeriodValues).AddDays(-1);
                }
                else
                {
                    //pMinDate = (fromDate ?? DateTime.Today);
                    //pMaxDate = (fromDate ?? DateTime.Today).AddDays(aPeriodValues + 1);
                }

                //var aMonthDiff = pMaxDate.Month + ((pMaxDate.Year - pMinDate.Year) * 12) - pMinDate.Month;
                //pMonthInterval = aMonthDiff + 1; // DO NOT MOVE

                var aWeekOfYearData = new List<WeekOfYear>();
                var a_list = new List<string>();
                int j = 0;
                for (int i = 0; i < DateAndTime.DateDiff(DateInterval.Day, pMinDate, pMaxDate) - 1; i++)
                {
                    var a_str = DateAndTime.DatePart(DateInterval.WeekOfYear, DateAndTime.DateAdd(DateInterval.Day, i, pMinDate)) + "," + DateAndTime.DateAdd(DateInterval.Day, i, pMinDate).Year;
                    if (!a_list.Contains(a_str))
                    {
                        a_list.Add(a_str);
                        aWeekOfYearData.Add(new WeekOfYear() { ID = j, Text = a_str });
                        j = j + 1;
                    }
                }

                //WeekOfYearData.Add(new WeekOfYear() { ID = 1, Text = "12,2020" });
                //await Task.Delay(10);
                //var aWeekOfYearData = new List<WeekOfYear>();
                //aWeekOfYearData.Add(new WeekOfYear() { ID = 1, Text = "12,2020" });
                return aWeekOfYearData;
            }
            catch (Exception q)
            {
                var aa = q.Message;
                return new List<WeekOfYear>();
                // throw;
            }

        }
        private static void ShowDates()
        {
            try
            {
                pShowing = $"From {pMinDate.ToString("dd/MM/yy")} To {pMaxDate.ToString("dd/MM/yy")}"; // 
            }
            catch (Exception qq)
            {
                var ss = qq.Message;
                pShowing = "";
            }
        }
        public static void SetGridPeriodInterval(string a_in)
        {
            try
            {
                pGridPeriodText = a_in;
                if (a_in == "Months")
                {
                    pGridPeriodValues = 2;
                    pGridPeriodMax = 13;
                    pGridPeriodMin = 1;
                    pGridPeriodStep = 1;
                }
                else
                {
                    pGridPeriodValues = 60;
                    pGridPeriodMax = 400;
                    pGridPeriodMin = 10;
                    pGridPeriodStep = 5;
                }
            }
            catch (Exception qq)
            {
                pGridPeriodText = "Months";
                pGridPeriodValues = 2;
                pGridPeriodMax = 13;
                pGridPeriodMin = 1;
                pGridPeriodStep = 1;
                var ss = qq.Message;
            }
        }
        private void GotoWeekOfYear()
        {
            try
            {
                //    If cmbWeekOfYear.SelectedIndex = 0 Then
                //   a_date = dtpBookingFrom.Value
                //Else
                //    a_date = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.WeekOfYear, Val(cmbWeekOfYear.Text) - 1, CDate("1 Jan " & Strings.Right(cmbWeekOfYear.Text, 4))))
                //End If
                // todayDate
            }
            catch (Exception aa)
            {
                var qq = aa.Message;
                //   throw;
            }
        }
        public  List<BookingData> GenerateBookings1(DateTime? fromDate, int pGridPeriodValues, string room_types = "", string booking_type = "", string agent_types = "")
        {
            try
            {
                var fromDateStr = "";
                var toDateStr = "";
                if (pGridPeriodText == "Months")
                {
                    pMinDate = new DateTime((fromDate ?? DateTime.Today).Year, (fromDate ?? DateTime.Today).Month, 1);
                    pMaxDate = pMinDate.AddMonths(pGridPeriodValues).AddDays(-1);
                    fromDateStr = pMinDate.ToString("dd MMM yyyy");
                    toDateStr = pMaxDate.ToString("dd MMM yyyy");
                    pMonthInterval = pGridPeriodValues;
                }
                else
                {
                    //pMinDate = (fromDate ?? DateTime.Today);
                    //pMaxDate = (fromDate ?? DateTime.Today).AddDays(pGridPeriodValues + 1);
                    //fromDateStr = pMinDate.ToString("dd MMM yyyy");
                    //toDateStr = pMaxDate.ToString("dd MMM yyyy");
                    //var aMonthDiff = pMaxDate.Month + ((pMaxDate.Year - pMinDate.Year) * 12) - pMinDate.Month;
                    //pMonthInterval = aMonthDiff + 1;
                }

                ShowDates();

                // var ss = pBookingSetting.gstrBookingStockDesc;
                var bookingList = new List<BookingData>();
                if (a_connEncEzy == "")
                {
                    return bookingList;
                }
                using (SqlConnection conn = new SqlConnection(a_connEncEzy))
                {
                    //////////////////////
                    // STEP 1 
                    conn.Open();
                    var aSQL = ""; // TOP (80) <<<<<<< -*****************************
                    aSQL = " Select tbl_Bookings.bookingnumber, arrivaldatetime, departuredatetime, firstname,surname,tbl_Bookings.ID,tbl_Bookings.roomcodeid, isnull(DonotMove,0) NoMove,status, tbl_contacts.mobile,tbl_contacts.email, tbl_contacts.Company, tbl_Bookings.AgentID   " + nl;
                    aSQL += "  , isnull(DualKey,0) as DualKey, isnull(DualKeyRoomCode,0) as DualKeyRoomCode, isnull(DualKeyAdditionalRecord,0) as DualkeyDummy,tbl_Bookings.roomtypeid, isnull(TBL_Creditors.reference,'') as AgentRef   " + nl;
                    aSQL += "  , tbl_Bookings.adults,tbl_Bookings.children,tbl_Bookings.infants,left(GuestRequestsComments,150) GuestRequestsComments " + nl;
                    aSQL += " FROM tbl_Bookings inner join  TBL_BookingGuestLink on tbl_Bookings.id = TBL_BookingGuestLink.bookingid inner join tbl_contacts  " + nl;
                    aSQL += "   on TBL_BookingGuestLink.contactid =  tbl_contacts.id left join TBL_Creditors on tbl_Bookings.AgentID = TBL_Creditors.ID   " + nl;
                    aSQL += "   where arrivaldatetime <= '" + toDateStr + "' and departuredatetime >=  '" + fromDateStr + "' " + nl;
                    if (room_types != "")
                    {
                        aSQL += " and tbl_Bookings.roomtypeid in (" + room_types + ") " + nl;
                    }
                    if (booking_type != "")
                    {
                        aSQL += booking_type + nl;
                    }
                    if (agent_types != "")
                    {
                        aSQL += "  and  tbl_Bookings.AgentID in  (" + agent_types + ") " + nl;
                        p_Show_AgentName = true;
                    }

                    aSQL += "    and tbl_Bookings.status <> 0  and tbl_Bookings.status <> 5 and tbl_Bookings.status <> 1 and tbl_Bookings.cancelled= 0 and TBL_BookingGuestLink.folionumber =1  and tbl_Bookings.roomcodeid  > 0   " + nl;
                    aSQL += "    order by  tbl_Bookings.roomcodeid, arrivaldatetime   " + nl;

                    using (SqlDataAdapter adapter1 = new SqlDataAdapter(aSQL, conn))
                    {
                        DataTable dt_allBookings = new DataTable();
                        adapter1.Fill(dt_allBookings);
                        var rowFound = dt_allBookings.Rows.Count;

                        if (rowFound > 0)
                        {
                            ////////////////////////////////////////////////////
                            //find deposit due records
                            DataTable dtDepoist = new DataTable();
                            var cmd = " select tbl_Bookings.id, tbl_Bookings.BookingNumber, isnull(DepositAmountRequired,0) DepositAmountRequired into #TmpBookingID  FROM tbl_Bookings   ";
                            cmd += " where arrivaldatetime <= '" + toDateStr + "' and departuredatetime >=  '" + fromDateStr + "' " + nl;
                            cmd += "       and tbl_Bookings.status < 3  and tbl_Bookings.cancelled= 0 " + nl;
                            cmd += "       and convert(nvarchar(8),tbl_Bookings.DepositRequiredBy,112) <= '" + DateTime.Today.ToString("yyyyMMdd") + "'" + nl;
                            cmd += "       and isnull(DepositAmountRequired,0) > 0 " + nl;
                            if (room_types != "")
                            {
                                cmd += " and tbl_Bookings.roomtypeid in (" + room_types + ") " + nl;
                            }
                            if (booking_type != "")
                            {
                                cmd += booking_type + nl;
                            }
                            if (agent_types != "")
                            {
                                cmd += "  and  tbl_Bookings.AgentID in  (" + agent_types + ") " + nl;
                            }
                            cmd += "; SELECT distinct B.BookingNumber " + nl;
                            cmd += "  FROM #TmpBookingID B left join  vGuestPayments on B.id = vGuestPayments.bookingid" + nl;
                            cmd += "  where B.DepositAmountRequired > isnull(vGuestPayments.AccountBalance,0); " + nl;
                            cmd += " drop table #TmpBookingID " + nl;
                            adapter1.SelectCommand.CommandText = cmd;
                            adapter1.Fill(dtDepoist);
                            var depositList = new List<string>();
                            for (int i = 0; i < dtDepoist.Rows.Count; i++)
                            {
                                depositList.Add(dtDepoist.Rows[i]["BookingNumber"].ToString());
                            }
                            ////////////////////////////////////////////////////

                            //var random = new Random();
                            for (int i = 0; i < rowFound; i++)
                            {
                                var ss12 = dt_allBookings.Rows[i]["arrivaldatetime"];
                                var aBooking = new BookingData();
                                aBooking.Id = int.Parse(dt_allBookings.Rows[i]["id"].ToString());
                                aBooking.BookingNumber = int.Parse(dt_allBookings.Rows[i]["BookingNumber"].ToString());
                                aBooking.Subject = "";
                                if (aBooking.BookingNumber == 194992)
                                {
                                    aBooking.BookingNumber = aBooking.BookingNumber;
                                }
                                aBooking.StartTime = Convert.ToDateTime(dt_allBookings.Rows[i]["arrivaldatetime"], CultureInfo.InvariantCulture);
                                aBooking.EndTime = Convert.ToDateTime(dt_allBookings.Rows[i]["departuredatetime"]);
                                aBooking.StartTimeOriginal = Convert.ToDateTime(dt_allBookings.Rows[i]["arrivaldatetime"]);
                                aBooking.EndTimeOriginal = Convert.ToDateTime(dt_allBookings.Rows[i]["departuredatetime"]);
                                aBooking.StartTimeValue = aBooking.StartTime;
                                aBooking.EndTimeValue = aBooking.EndTime;
                                // var a_nights = DateAndTime.DateDiff(DateInterval.Day, Convert.ToDateTime(Convert.ToDateTime(dt_allBookings.Rows[i]["arrivaldatetime"]).ToString("yyyy/MM/dd")), Convert.ToDateTime(Convert.ToDateTime(dt_allBookings.Rows[i]["departuredatetime"]).ToString("yyyy/MM/dd")));
                                //var aTravelDates = $"{Convert.ToDateTime(dt_allBookings.Rows[i]["arrivaldatetime"]).ToString("dd/MM/yy")} - {Convert.ToDateTime(dt_allBookings.Rows[i]["departuredatetime"]).ToString("dd/MM/yy")} ({a_nights.ToString()} Nts)";
                                // aBooking.BookingDates = aTravelDates;

                                //if ((pBookingGridDisplaySettings.gBookingGrid_Sort ?? "2") == "0")
                                //{
                                //    aBooking.RoomTypeId = 1;
                                //}
                                //else
                                //{
                                //    aBooking.RoomTypeId = int.Parse(dt_allBookings.Rows[i]["roomtypeid"].ToString());
                                //}
                                aBooking.RoomTypeId = int.Parse(dt_allBookings.Rows[i]["roomtypeid"].ToString());
                                aBooking.RoomCodeId = int.Parse(dt_allBookings.Rows[i]["roomcodeid"].ToString());
                                aBooking.RoomCodeIdOriginal = int.Parse(dt_allBookings.Rows[i]["roomcodeid"].ToString()); //for when moved tracking
                                aBooking.RoomTypeIdOriginal = int.Parse(dt_allBookings.Rows[i]["roomtypeid"].ToString()); //for when moved tracking

                                aBooking.IsReadonly = bool.Parse(dt_allBookings.Rows[i]["NoMove"].ToString());

                                var aStatus = dt_allBookings.Rows[i]["status"].ToString();
                                aBooking.Status = int.Parse(aStatus);
                                var arrive = Convert.ToDateTime(dt_allBookings.Rows[i]["arrivaldatetime"]).ToString("yyMMdd");
                                var depart = Convert.ToDateTime(dt_allBookings.Rows[i]["departuredatetime"]).ToString("yyMMdd");
                                var agentID = dt_allBookings.Rows[i]["AgentID"].ToString();

                                var depositDue = false;
                                if (depositList.Contains(dt_allBookings.Rows[i]["BookingNumber"].ToString()))
                                {
                                    depositDue = true;
                                }
                                var statusText = "";
                                aBooking.BookingBarColor = ReturnBKColour(aStatus, arrive, depart, depositDue, agentID, out statusText);

                                var a_agentName = "";
                                var aBookingBarText = "";
                                if (p_Show_AgentName && dt_allBookings.Rows[i]["AgentRef"].ToString().ToLower() != "direct")
                                {
                                    a_agentName = " (" + dt_allBookings.Rows[i]["AgentRef"].ToString() + ")";
                                }

                                if (dt_allBookings.Rows[i]["Company"].ToString().Trim() != "")
                                {

                                    if (dt_allBookings.Rows[i]["surname"].ToString().Trim() != "")
                                    {
                                        aBookingBarText = dt_allBookings.Rows[i]["surname"].ToString() + " - " + dt_allBookings.Rows[i]["Company"].ToString() + " #" + dt_allBookings.Rows[i]["BookingNumber"].ToString() + a_agentName;
                                    }
                                    else
                                    {
                                        aBookingBarText = dt_allBookings.Rows[i]["Company"].ToString() + " #" + dt_allBookings.Rows[i]["BookingNumber"].ToString() + a_agentName;
                                    }
                                }
                                else
                                {
                                    aBookingBarText = dt_allBookings.Rows[i]["surname"].ToString() + " #" + dt_allBookings.Rows[i]["BookingNumber"].ToString() + a_agentName;
                                }
                                aBooking.BookingBarText = aBookingBarText;
                                aBooking.GuestName = $"{dt_allBookings.Rows[i]["firstname"].ToString()} {dt_allBookings.Rows[i]["surname"].ToString()}";
                                var a_pax = dt_allBookings.Rows[i]["adults"].ToString() + " Adults,";
                                if (int.Parse(dt_allBookings.Rows[i]["children"].ToString()) > 0)
                                    a_pax += dt_allBookings.Rows[i]["children"].ToString() + " Children,";
                                if (int.Parse(dt_allBookings.Rows[i]["infants"].ToString()) > 0)
                                    a_pax += dt_allBookings.Rows[i]["infants"].ToString() + " Infants,";
                                aBooking.Pax = a_pax.TrimEnd(',');

                                aBooking.StatusText = statusText;
                                aBooking.Mobile = dt_allBookings.Rows[i]["mobile"].ToString();
                                var aRoom_Code = RoomCodeData.SingleOrDefault(x => x.Id == int.Parse(dt_allBookings.Rows[i]["roomcodeid"].ToString()))?.RoomCodeText ?? "";
                                var aType_Code = RoomTypeData.SingleOrDefault(x => x.Id == int.Parse(dt_allBookings.Rows[i]["roomtypeid"].ToString()))?.RoomTypeText ?? "";
                                var aDualKey_CodeName = RoomCodeData.SingleOrDefault(x => x.Id == int.Parse(dt_allBookings.Rows[i]["roomcodeid"].ToString()))?.DualKeyLinkedRoomCodeName ?? "";

                                aBooking.RoomCodeText = aRoom_Code;
                                aBooking.RoomTypeText = aType_Code;
                                aBooking.Comments = dt_allBookings.Rows[i]["GuestRequestsComments"].ToString();
                                aBooking.DualKeyDummy = bool.Parse(dt_allBookings.Rows[i]["DualKeyDummy"].ToString() ?? "false");
                                aBooking.DualKey = bool.Parse(dt_allBookings.Rows[i]["DualKey"].ToString() ?? "false");
                                aBooking.DualKeyLinkedRoomID = int.Parse(dt_allBookings.Rows[i]["DualKeyRoomCode"].ToString() ?? "0");
                                aBooking.DualKeyLinkedRoomCodeName = aDualKey_CodeName;
                                //aBooking.BookingTipText = "<div><label>Description 6 </label>: <span>gok hoho</span></div>";
                                bookingList.Add(aBooking);

                            }

                            var ss1 = 3;
                            ss1 = ss1 + 4;
                        }

                        dt_allBookings.Dispose();

                        //////////////////////////////////////////////////////
                        // add maintenace
                        DataTable dt_mainte = new DataTable();
                        var cmd1 = "select tbl_Bookings.bookingnumber, arrivaldatetime, departuredatetime, 'Maintenance','Maintenance',tbl_Bookings.ID,tbl_Bookings.roomcodeid, roomtypeid,isnull(DonotMove,0),status " + nl;
                        cmd1 += "  FROM tbl_Bookings" + nl;
                        cmd1 += " where arrivaldatetime <= '" + toDateStr + "' and departuredatetime >=  '" + fromDateStr + "' " + nl;
                        cmd1 += "  and tbl_Bookings.status <> 0 and tbl_Bookings.status = 6 and tbl_Bookings.cancelled= 0 and tbl_Bookings.roomcodeid  > 0" + nl;
                        cmd1 += "  and bookingnumber =0  " + nl;
                        if (room_types != "")
                        {
                            cmd1 += " and tbl_Bookings.roomtypeid in (" + room_types + ") " + nl;
                        }
                        if (booking_type != "")
                        {
                            cmd1 += booking_type + nl;
                        }
                        if (agent_types != "")
                        {
                            cmd1 += "  and  tbl_Bookings.AgentID in  (" + agent_types + ") " + nl;
                        }
                        cmd1 += " order by  tbl_Bookings.roomcodeid, arrivaldatetime ";
                        adapter1.SelectCommand.CommandText = cmd1;
                        adapter1.Fill(dt_mainte);
                        for (int i = 0; i < dt_mainte.Rows.Count; i++)
                        {
                            var aBooking = new BookingData();
                            aBooking.Id = int.Parse(dt_mainte.Rows[i]["ID"].ToString());
                            aBooking.Subject = "Maintenance : " + dt_mainte.Rows[i]["ID"].ToString();
                            aBooking.StartTime = Convert.ToDateTime(dt_mainte.Rows[i]["arrivaldatetime"]);
                            aBooking.EndTime = Convert.ToDateTime(dt_mainte.Rows[i]["departuredatetime"]);
                            aBooking.StartTimeOriginal = Convert.ToDateTime(dt_mainte.Rows[i]["arrivaldatetime"]);
                            aBooking.EndTimeOriginal = Convert.ToDateTime(dt_mainte.Rows[i]["departuredatetime"]);
                            aBooking.StartTimeValue = aBooking.StartTime;
                            aBooking.EndTimeValue = aBooking.EndTime;
                            //if ((pBookingGridDisplaySettings.gBookingGrid_Sort ?? "2") == "0")
                            //{
                            //    aBooking.RoomTypeId = 1;
                            //}
                            //else
                            //{
                            //    aBooking.RoomTypeId = int.Parse(dt_mainte.Rows[i]["roomtypeid"].ToString());
                            //}
                            aBooking.RoomTypeId = int.Parse(dt_mainte.Rows[i]["roomtypeid"].ToString());
                            aBooking.RoomCodeId = int.Parse(dt_mainte.Rows[i]["roomcodeid"].ToString());
                            aBooking.IsReadonly = true;

                            var aStatus = "6"; // dt_mainte.Rows[i]["status"].ToString();
                            aBooking.Status = 6;
                            var arrive = Convert.ToDateTime(dt_mainte.Rows[i]["arrivaldatetime"]).ToString("yyMMdd");
                            var depart = Convert.ToDateTime(dt_mainte.Rows[i]["departuredatetime"]).ToString("yyMMdd");
                            var agentID = "0";//dt_mainte.Rows[i]["AgentID"].ToString();

                            var depositDue = false;
                            var statusText = "";
                            aBooking.BookingBarColor = ReturnBKColour(aStatus, arrive, depart, depositDue, agentID, out statusText);

                            bookingList.Add(aBooking);
                        }
                        //////////////////////////////////////////////////////
                    }
                }
                return bookingList; // resources;
            }
            catch (Exception tt)
            {

                var qq = tt.Message;
                return new List<BookingData>();
                // throw;
            }

        }
        // Dim a_style_no As Integer = ReturnBKColour(CDate(a_rows(i).ItemArray(1)), CDate(a_rows(i).ItemArray(2)), a_rows(i).ItemArray(8).ToString, a_rows(i).ItemArray(5).ToString, 0, a_rows(i).Item("AgentID").ToString, a_overdue)
        private static string ReturnBKColour(string aStatus, string dArrive, string dDepart, bool dDepositOverDue, string pAgentCode, out string statusText)
        {
            //reimaster-> 1 = Enquiry, 2 = Booking, 3 = In House, 4 = Departed, 5 = Cancelled, 6 = Maintenance

            //'0 arrival today Color.LawnGreen , 1 arrival overdue Color.DarkOrange, 2 booking (in ezyrez booking with deposit ) Color.Aqua 
            //3 booking with  deposit due (in ezyrez withut deposit) Color.RoyalBlue (white), 4 inhouse  Color.LightCoral , 5 depart today Color.Firebrick (white)
            //6 depart over due Color.DarkOrchid (white), 7 departed Color.DarkSeaGreen, 8 maintenabce Color.Silver , 9  LightGray, 10 Yellow
            //DarkOrchid RoyalBlue Firebrick
            try
            {
                switch (aStatus)
                {
                    case "2":// booking
                        if (dArrive == DateTime.Today.ToString("yyMMdd"))
                        {
                            statusText = "Arrive Today";
                            return "LawnGreen".ToLower();
                        } //0
                        else if (dDepart == DateTime.Today.ToString("yyMMdd"))
                        {
                            statusText = "Depart Today";
                            return "Firebrick".ToLower();
                        }//5
                        else if (int.Parse(dArrive) < int.Parse(DateTime.Today.ToString("yyMMdd")))
                        {
                            statusText = "Arrive Overdue"; return "DarkOrange".ToLower();
                        } //1
                        else if (pAgentCode != "50000")
                        {
                            statusText = "Booking"; return "LightSeaGreen".ToLower();
                        }//2 Aqua
                        else if (pAgentCode == "50000" && dDepositOverDue)
                        {
                            statusText = "Booking With Overdue Deposit"; return "RoyalBlue".ToLower();
                        }//3
                        else
                        {
                            statusText = "Booking"; return "LightSeaGreen".ToLower();
                        }
                    case "3": //inhouse
                        if (dDepart == DateTime.Today.ToString("yyMMdd"))
                        {
                            statusText = "Depart Today"; return "Firebrick".ToLower();
                        } //5
                        else if (int.Parse(dDepart) < int.Parse(DateTime.Today.ToString("yyMMdd")))
                        {
                            statusText = "Depart overdue"; return "DarkOrchid".ToLower();
                        }//6
                        else
                        { statusText = "In House"; return "LightCoral".ToLower(); }//4
                    case "4": //departed
                        statusText = "Departed"; return "DarkSeaGreen".ToLower();
                    case "6": //mainte
                        statusText = "Maintenance";
                        return "Silver".ToLower();
                    case "1": //enquiry
                        statusText = "Enquiry";
                        return "LightGray".ToLower();
                    case "5": //cancelled
                        statusText = "Cancelled";
                        return "Yellow".ToLower();
                    default://shoud not reach
                        statusText = "Booking?";
                        return "LawnGreen".ToLower();
                }
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
                //throw;
                statusText = "Booking?";
                return "LawnGreen".ToLower();
            }
        }
        public bool Check_Booking_Overlap(BookingData a_booking, string a_roomcode, string a_roomcodeName, string a_roomtypeid, string a_roomtypename, DateTime a_start, DateTime a_end, string a_id = "", bool a_resize = false, bool a_no_message = false)
        {   // is ovelapping?  // there is an extra para,eter a_booking and a_to_dualkey -> different to winapp
            try
            {
                pShowChangeTariffMessage = false;
                if (a_booking.StartTime.ToString("yyyyMMdd") == a_booking.StartTimeOriginal.ToString("yyyyMMdd") && a_booking.EndTime.ToString("yyyyMMdd") == a_booking.EndTimeOriginal.ToString("yyyyMMdd") &&
                      a_booking.RoomCodeId == a_booking.RoomCodeIdOriginal && !a_resize)
                {
                    return false; // same location, did not move or resize
                }
                var a_sql = " SELECT count(ID) FROM TBL_Bookings WHERE "; // 'ID , BookingNumber
                a_sql += " roomcodeid = " + a_roomcode + " AND Cancelled = 0 AND status <> 1 and ";
                a_sql += "  ArrivalDateTime  < '" + a_end.ToString("yyyy MMM dd HH:mm") + "' ";
                a_sql += " AND DepartureDateTime > '" + a_start.ToString("yyyy MMM dd HH:mm") + "' ";
                if (a_id != "")
                { a_sql += " AND ID <> " + a_id; }

                var result = QExec(a_sql, a_connEncEzy);
                if (!result.Contains("errror") && result != "")
                {
                    var a_count = int.Parse(result);
                    if (a_count > 0)
                    { return true; }
                    else
                    {
                        if (a_booking.RoomTypeId == a_booking.RoomTypeIdOriginal && !a_resize)
                        {
                            return false; //'if the same roomtype and updown- DO not show message
                        }
                        else
                        {
                            if (a_no_message || p_To_DualKey)
                            {
                                return false; //show no message and exit here -just check if it 's overlap and this case it's not
                            }
                            var a_str = "";
                            var a_from_type = QExec("select description from tbl_roomtypes where id = " + a_booking.RoomTypeIdOriginal, a_connEncEzy);
                            var a_to_type = QExec("select description from tbl_roomtypes where id = " + a_roomtypeid, a_connEncEzy);

                            a_str = "The booking is currently for a " + a_from_type + " " + gstrBookingStockDesc.ToLower() + "." + nl;
                            a_str += "The selected " + gstrBookingStockDesc.ToLower() + " you are moving this booking to is a " + a_to_type + " " + gstrBookingStockDesc.ToLower() + "." + nl;
                            a_str += "Click OK to continue with the change to a different " + gstrBookingStockDesc.ToLower() + " type." + nl;

                            if (a_resize)  //resizing
                            {
                                a_str = "";
                                if (a_booking.StartTime.ToString("yyyyMMdd HH:mm") != a_booking.StartTimeOriginal.ToString("yyyyMMdd HH:mm"))
                                {
                                    if (a_booking.StartTime.ToString("yyyyMMdd") == a_booking.StartTimeOriginal.ToString("yyyyMMdd"))
                                    {
                                        return false; //'If change in arrival time does not cross into the prior day, just allow change with no message
                                    }
                                    else
                                    {
                                        a_str = "The arrival date / time has changed to" + nl + a_start.ToString("dddd dd/MM/yyyy h:mm tt") + ".";
                                    }
                                }
                                if (a_booking.EndTime.ToString("yyyyMMdd HH:mm") != a_booking.EndTimeOriginal.ToString("yyyyMMdd HH:mm"))
                                {
                                    if (a_booking.EndTime.ToString("yyyyMMdd") == a_booking.EndTimeOriginal.ToString("yyyyMMdd"))
                                    {
                                        return false; //'If change in departure time does not cross into the prior day, just allow change with no message
                                    }
                                    else
                                    {
                                        a_str = "The departure date / time has changed to" + nl + a_end.ToString("dddd dd/MM/yyyy h:mm tt") + ".";
                                    }
                                }
                            }

                            //---------------------------------------
                            // initialise
                            BookingChangeContent = "";
                            BookingChangeCheckBox3 = true;
                            BookingChangeCheckBox3 = true;
                            BookingChangeCheckBox3 = true;
                            BookingChangeCheckBox1Visible = true; //show when resize but do not show when updown
                            BookingChangeCheckBox2Visible = true;
                            BookingChangeCheckBox3Visible = true; //show when resize but do not show when updown
                            //-----------------------------------------
                            if (a_resize)  //resizing
                            {
                                p_MoveDirection = "sideside";
                                pIsRoomTypeChange = false;
                            }
                            else
                            {
                                p_MoveDirection = "updown";
                                pIsRoomTypeChange = true;
                                pShowChangeTariffMessage = true;

                                BookingChangeCheckBox1Visible = false; //show when resize but do not show when updown
                                BookingChangeCheckBox3Visible = false; //show when resize but do not show when updown
                            }

                            if (a_booking.Status == 6) //maintenace
                            {
                                BookingChangeCheckBox1Visible = false;
                                BookingChangeCheckBox2Visible = false;
                                BookingChangeCheckBox3Visible = false;
                            }
                            if (a_booking.Status == 1) // enquiry
                            {
                                BookingChangeCheckBox3Visible = false;
                                BookingChangeCheckBox3 = false;
                            }

                            BookingChangeHeader = "Confirm Change Details For " + a_booking.GuestName;
                            if (pIsRoomTypeChange)
                            {
                                BookingChangeHeader = "Confirm Change In " + gstrBookingStockDesc + " Type For " + a_booking.GuestName;
                                var sSQL = "SELECT ISNULL(UntickRemoveAndReapplyLinkedCharges,0) FROM TBL_UserOptions WHERE UserID = " + glngUserID;
                                var bUnTickCheckBox2 = QExec(sSQL, a_connEncEzy);
                                if (bool.Parse(bUnTickCheckBox2))
                                {
                                    BookingChangeCheckBox2 = false;
                                }
                            }

                            BookingChangeContent = $"{a_str} Continue with the change to this " + ((a_booking.Status == 6) ? "maintenance?" : "booking?");
                            BookingChangeImage = "fa-question-circle";
                            BookingChangeShowBottomCloseButton = true;
                            BookingChangeHeaderBackground = "cornflowerblue";
                            BookingChangeHeaderIconColor = "green";
                            BookingChangeBookingItem = a_booking;  // data to refresh on teh grid

                            return false;
                        }
                    }
                }
                else
                {
                    return true;
                }

            }
            catch (Exception q3)
            {
                var a = q3.Message;
                return true;
            }
        }
        public bool Is_Booking_OverlapQuick(string a_roomcode, string a_roomtypeid, DateTime a_start, DateTime a_end, string a_bookingid, bool a_dateOnly = false)
        {
            try
            {
                var a_sql = " SELECT count(ID) FROM TBL_Bookings WHERE "; // 'ID , BookingNumber
                a_sql += " roomcodeid = " + a_roomcode + " AND Cancelled = 0 AND status <> 1 and " + nl;
                if (a_dateOnly)// 'a_start a_end must be yyyymmdd format
                {
                    a_sql += " convert( nvarchar(8), ArrivalDateTime,112)  < '" + a_end.ToString("yyyyMMdd") + "' " + nl;
                    a_sql += " AND convert( nvarchar(8), DepartureDateTime,112) > '" + a_start.ToString("yyyyMMdd") + "' " + nl;
                }
                else
                {
                    a_sql += "  ArrivalDateTime  < '" + a_end.ToString("yyyy MMM dd HH:mm") + "' " + nl;
                    a_sql += " AND DepartureDateTime > '" + a_start.ToString("yyyy MMM dd HH:mm") + "' " + nl;
                }
                if (a_bookingid != "")
                { a_sql += " AND ID <> " + a_bookingid; }

                //testSQLStr = a_sql;
                var result = QExec(a_sql, a_connEncEzy);
                if (!result.Contains("errror") && result != "")
                {
                    var a_count = int.Parse(result);
                    if (a_count == 0)
                    { return false; }
                    else
                    { return true; }
                }
                else
                {
                    return true;
                }

            }
            catch (Exception q)
            {
                var w = q.Message;
                return true;
                //throw;
            }
        }
        public int DualKey_Conflict_When_Resize(BookingData a_booking, DateTime a_start, DateTime a_end)
        {
            try
            {
                var a_conflict = 0;
                var aList = QExecList("select roomcodeid, roomtypeid, id from tbl_bookings where bookingnumber = " + a_booking.BookingNumber + " and id <> " + a_booking.Id, a_connEncEzy, 3);
                if (aList.Count > 0)
                {
                    var other_room_code = aList[0];
                    var other_room_type = aList[1];
                    var other_booking_id = aList[2];
                    var sSQL = "SELECT COUNT(ID) FROM TBL_Bookings" + nl
                             + " WHERE RoomCodeID = " + a_booking.DualKeyLinkedRoomID + " AND Cancelled = 0 AND status <> 1 " + nl
                             + " AND ArrivalDateTime < '" + a_end.ToString("dd MMM yyyy HH:mm") + "' AND DepartureDateTime > '" + a_start.ToString("dd MMM yyyy HH:mm") + "'" + nl
                             + " AND ID <> " + other_booking_id;
                    a_conflict = int.Parse(QExec(sSQL, a_connEncEzy));  // check dual key conflict here
                }
                else
                {
                    a_conflict = -1; // no match dual key found even though it says dual key - error - so do not let resize in case error!
                }
                return a_conflict;
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
                return 1; // assume there's dual key , so do not let resize.
            }
        }
        public string Enquiry_Conflict_Check(BookingData a_booking, DateTime a_start, DateTime a_end)
        {
            try
            {
                var a_result = "";
                var sSQL = "SELECT COUNT(ID) FROM TBL_Bookings"
                         + " WHERE RoomCodeID = " + a_booking.RoomCodeId + " AND Cancelled = 0 AND Enquiry = 1 "
                         + " AND ArrivalDateTime < '" + a_end.ToString("dd MMM yyyy") + "' AND DepartureDateTime > '" + a_start.ToString("dd MMM yyyy") + "'"
                         + " AND ID <> " + a_booking.Id;
                var a_conflict = int.Parse(QExec(sSQL, a_connEncEzy));  // check enquiry conflict here
                if (a_conflict > 0)
                {
                    if (a_booking.Status == 6) // maintenance
                    {
                        a_result = "There is an existing enquiry saved that conflicts with the date / time you're changing for this maintenance.";
                    }
                    else
                    {
                        a_result = "There is an existing enquiry saved that conflicts with the date / time you're changing for this booking.";
                    }
                }
                return a_result;
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
                return ""; // assume there's conflict enquiry
            }
        }
        public async Task<bool> MoveUpDown_Check_Save(BookingData aBooking, DateTime a_start, DateTime a_end)
        { // due to complexity of the rate, use options to choose. just opne a booking let the user to do the rate changes etc. when booking is mved up down
            try
            {
                var aBar = aBooking;
                var destRoom = RoomCodeData.FirstOrDefault(x => x.Id == aBar.RoomCodeId);
                if (destRoom != null)
                {
                    var dest_dualkey = destRoom.DualKey;
                    if (aBar.DualKey && dest_dualkey && p_To_DualKey) //from and destination are dual keys
                    {
                        var a_room_code4 = destRoom.Id + "," + destRoom.DualKeyLinkedRoomID;
                        var a_other_booking = QExecList("select roomcodeid, roomtypeid, id from tbl_bookings where bookingnumber = " + aBar.BookingNumber + " and id <> " + aBar.Id, a_connEncEzy, 3);
                        var a_linked_roomtype4 = QExec("select RoomTypeID from tbl_property where id = " + destRoom.DualKeyLinkedRoomID, a_connEncEzy);// 'destination other roomtype
                        var a_room_type4 = destRoom.RoomListRoomType_Id + "," + a_linked_roomtype4;
                        var a_booking_id4 = aBar.Id + "," + a_other_booking[2];
                        var a_linked_roomcode4 = destRoom.DualKeyLinkedRoomID + "," + destRoom.Id;
                        Save_Booking_ThatMoved2(a_room_code4, a_linked_roomcode4, a_room_type4, "", a_start, a_end, a_booking_id4, false, false, true);
                    }
                    if (aBar.DualKey && dest_dualkey && !p_To_DualKey) //from dual key to one of dual key
                    {
                        var a_other_booking = QExecList("select roomcodeid, roomtypeid, id from tbl_bookings where bookingnumber = " + aBar.BookingNumber + " and id <> " + aBar.Id, a_connEncEzy, 3);
                        QExec("delete from tbl_bookings where id = " + a_other_booking[2], a_connEncEzy); //'delete one dualkey room
                        QExec("delete from TBL_BookingGuestLink where bookingid = " + a_other_booking[2], a_connEncEzy);
                        QExec("update tbl_bookings set DualKey = 0,DualKeyRoomCode =0,DualKeyAdditionalRecord =0 where id = " + aBar.Id, a_connEncEzy);
                        Save_Booking_ThatMoved(destRoom.Id.ToString(), destRoom.RoomCodeText, destRoom.RoomListRoomType_Id.ToString(), destRoom.RoomTypeText, a_start, a_end, aBar.Id.ToString());
                    }

                    if (!aBar.DualKey && dest_dualkey && p_To_DualKey) //'only destination is dual key and said yes to move to dual
                    {
                        //'create a room also
                        var a_room_code4 = destRoom.DualKeyLinkedRoomID;
                        var a_room_type4 = QExec("select RoomTypeID from tbl_property where id = " + a_room_code4.ToString(), a_connEncEzy);
                        var a_room_linked_code4 = destRoom.Id.ToString();
                        Create_A_Booking_For_Dualkey_Dummy(aBar.Id.ToString(), a_room_code4.ToString(), a_room_type4, a_room_linked_code4);
                        Save_Booking_ThatMoved2(destRoom.Id.ToString(), destRoom.DualKeyLinkedRoomID.ToString(), destRoom.RoomListRoomType_Id.ToString(), destRoom.RoomTypeText, a_start, a_end, aBar.Id.ToString(), true, false, true);
                    }
                    if (aBar.DualKey && !dest_dualkey) //'from dual key to non-dual key
                    {
                        var a_other_booking = QExecList("select roomcodeid, roomtypeid, id from tbl_bookings where bookingnumber = " + aBar.BookingNumber + " and id <> " + aBar.Id, a_connEncEzy, 3);
                        QExec("delete from tbl_bookings where id = " + a_other_booking[2], a_connEncEzy); //'delete one dualkey room
                        QExec("delete from TBL_BookingGuestLink where bookingid = " + a_other_booking[2], a_connEncEzy);
                        QExec("update tbl_bookings set DualKey = 0,DualKeyRoomCode =0,DualKeyAdditionalRecord =0 where id = " + aBar.Id, a_connEncEzy);
                        Save_Booking_ThatMoved(destRoom.Id.ToString(), destRoom.RoomCodeText, destRoom.RoomListRoomType_Id.ToString(), destRoom.RoomTypeText, a_start, a_end, aBar.Id.ToString());
                    }
                    // nondual to non-dual or (from non dual to dual but said 'No' to dual
                    if ((!aBar.DualKey && !dest_dualkey) || (!aBar.DualKey && dest_dualkey && !p_To_DualKey))
                    {
                        Save_Booking_ThatMoved(destRoom.Id.ToString(), destRoom.RoomCodeText, destRoom.RoomListRoomType_Id.ToString(), destRoom.RoomTypeText, a_start, a_end, aBar.Id.ToString());
                    }

                    //unlike winapp, i moved up here from the bottom , update propertyid in tbl_tasks
                    QExec("update tbl_tasks set propertyid = " + destRoom.Id.ToString() + " where bookingid = " + aBar.Id, a_connEncEzy);

                    if (pShowChangeTariffMessage)
                    {
                        var a_msg = "The booking has been moved to a different type of " + gstrBookingStockDesc.ToLower() + ". Do you want to assign a different rate to this booking?";
                        var aYes = await JSRuntime.InvokeAsync<bool>("openABookingMessage", "123", a_msg);
                        if (aYes)
                        {   // to do 
                            //open a booking- i do not check for dualkey dummy like winapp, because, you cnnot move from dualkey dummy
                            a_msg = "" + a_msg;
                        }
                    }

                    await Task.Delay(1);
                    return true;
                }
                else
                {
                    await Task.Delay(1);
                    return false;
                }
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
                return false; // 
            }
        }

        public async Task<bool> Check_3_Options_And_Save(BookingData aBooking, bool aReTariff, bool aReCharge, bool aReHousekeeping, bool chk1Visible, bool chk2Visible, bool chk3Visible)
        {  //for side to side move -
            try
            {
                var startLocal = ToReiLocal(aBooking.StartTime, pClientTimeZone);//var myTimezone = GetTimeZoneInfo("Queensland");
                var endLocal = ToReiLocal(aBooking.EndTime, pClientTimeZone);
                if (chk1Visible)
                {
                    if (aReTariff)
                    {
                        var c1_sql = " Select sum ( TariffAmount + " + nl
                                   + "      case when isnull(RT.ChargeExtraPaxAtAdultRate,0) = 1 then  " + nl
                                   + " dbo.fnTariffExtraPax_1 ( isnull(RT.MaxIncludedPax,2), isnull(RT.ExtraAdultCharge,2), isnull(RT.ExtraAdultCharge,2), isnull(RT.ExtraAdultCharge,2), 0,B.Adults,B.Children,B.Infants) else " + nl
                                   + "   dbo.fnTariffExtraPax_1 ( isnull(RT.MaxIncludedPax,2), isnull(RT.ExtraAdultCharge,2), isnull(RT.ExtraChildCharge,2), isnull(RT.ExtraInfantCharge,2),0,B.Adults,B.Children,B.Infants ) end ) " + nl
                                   + " from TBL_Bookings as B inner join TBL_Rates as R on B.RateID = R.ID and B.ID = " + aBooking.Id + " inner join TBL_RoomTypes AS RT on R.TariffID = RT.ID " + nl
                                   + " inner join TBL_Tariffs as T on  RT.ID = T.RoomTypeID and TariffDate < '" + endLocal.ToString("dd MMM yyyy") + "' AND TariffDate >= '" + startLocal.ToString("dd MMM yyyy") + "'";

                        var a_result2 = QExec(c1_sql, a_connEncEzy);
                        var a_result3 = QExec("update TBL_Bookings set TotalAccommodation  = " + double.Parse(a_result2) + " Where TBL_Bookings.id= " + aBooking.Id, a_connEncEzy);
                        Recalc_Primary_Accomm_Amount(a_result2, aBooking.RoomCodeId.ToString(), aBooking.Id.ToString());
                    }
                }
                if (chk2Visible)
                {
                    if (aReCharge)
                    {
                        var iRateID = 0;
                        var sSQL = "SELECT RateID FROM TBL_Bookings WHERE ID = " + aBooking.Id;
                        iRateID = int.Parse(QExec(sSQL, a_connEncEzy));
                        var iPrimaryContactID = 0;
                        sSQL = "SELECT ContactID FROM TBL_BookingGuestLink WHERE BookingID = " + aBooking.Id + " AND FolioNumber = 1;";
                        iPrimaryContactID = int.Parse(QExec(sSQL, a_connEncEzy));
                        if (iRateID > 0)
                        {
                            HideOriginalGuestCharges(aBooking.Id);
                            sSQL = "SELECT isnull(Corporate,0) FROM TBL_Bookings WHERE ID = " + aBooking.Id;
                            var aCorporateBooking = bool.Parse(QExec(sSQL, a_connEncEzy));
                            if (aCorporateBooking)
                            {
                                var a_newStart = startLocal.ToString("dd MMM yyyy");
                                var a_newEnd = endLocal.ToString("dd MMM yyyy");
                                ApplyRateCharges(iRateID, 2, aBooking.Id, iPrimaryContactID, 0, 0, false, true, true, out a_newStart, out a_newEnd, true, 0, false, true, 0);
                            }
                            else
                            {
                                var a_newStart = startLocal.ToString("dd MMM yyyy");
                                var a_newEnd = endLocal.ToString("dd MMM yyyy");
                                ApplyRateCharges(iRateID, 2, aBooking.Id, iPrimaryContactID, 0, 0, false, true, true, out a_newStart, out a_newEnd, true, 0, false, false, 0);
                            }
                        }
                    }
                }
                var reHouse = false;
                if (chk3Visible)
                {
                    if (aReHousekeeping)
                    {
                        reHouse = true;
                    }
                }

                if (reHouse)
                {
                    var iRateID = 0;
                    var sSQL = "SELECT RateID FROM TBL_Bookings WHERE ID = " + aBooking.Id;
                    iRateID = int.Parse(QExec(sSQL, a_connEncEzy));
                    var iPrimaryContactID = 0;
                    sSQL = "SELECT ContactID FROM TBL_BookingGuestLink WHERE BookingID = " + aBooking.Id + " AND FolioNumber = 1;";
                    iPrimaryContactID = int.Parse(QExec(sSQL, a_connEncEzy));
                    if (iRateID > 0)
                    {
                        sSQL = "DELETE FROM TBL_Tasks WHERE BookingID = " + aBooking.Id + " AND Status = 'Not Started'";
                        var result1 = QExec(sSQL, a_connEncEzy);

                        var a_newStart = startLocal.ToString("dd MMM yyyy");
                        var a_newEnd = endLocal.ToString("dd MMM yyyy");
                        ApplyRateHousekeeping(iRateID, aBooking.Id, iPrimaryContactID, 0, 0, 0, false, true, out a_newStart, out a_newEnd, false, 0, 0);
                    }
                }
                else
                {
                    var sSQL = "UPDATE TBL_Tasks SET DueDate = '" + endLocal.ToString("dd MMM yyyy") + "', StartDate = '" + endLocal.ToString("dd MMM yyyy") + "', EndDate = '" + endLocal.ToString("dd MMM yyyy") + "'"
                                + " WHERE BookingID = " + aBooking.Id + " AND SubType In (3,4)";
                    var result1 = QExec(sSQL, a_connEncEzy);
                    sSQL = "UPDATE TBL_Tasks SET DueDate = '" + startLocal.ToString("dd MMM yyyy") + "', StartDate = '" + startLocal.ToString("dd MMM yyyy") + "', EndDate = '" + startLocal.ToString("dd MMM yyyy") + "'"
                           + " WHERE BookingID = " + aBooking.Id + " AND SubType In (6)";
                    var result2 = QExec(sSQL, a_connEncEzy);
                }

                /////////////////////////////////////////////////
                // NOW do the booking table saving
                var all_ok = false;
                var is_mainte = false;
                if (aBooking.Status == 6)
                    is_mainte = true;

                if (aBooking.DualKey) //' it is dualkey and its dragging
                {
                    var aList = QExecList("select roomcodeid, roomtypeid, id from tbl_bookings where bookingnumber = " + aBooking.BookingNumber + " and id <> " + aBooking.Id, a_connEncEzy, 3);
                    if (aList.Count > 0)
                    {
                        var other_room_code = aList[0];
                        var other_room_type = aList[1];
                        var other_booking_id = aList[2];
                        all_ok = Save_Booking_ThatMoved(aBooking.RoomCodeId.ToString() + "," + other_room_code, aBooking.RoomCodeText, aBooking.RoomTypeId + "," + other_room_type, aBooking.RoomTypeText, startLocal, endLocal, aBooking.Id.ToString() + "," + other_booking_id, true, is_mainte, true);
                    }
                }
                else
                {
                    all_ok = Save_Booking_ThatMoved(aBooking.RoomCodeId.ToString(), aBooking.RoomCodeText, aBooking.RoomTypeId.ToString(), aBooking.RoomTypeText, startLocal, endLocal, aBooking.Id.ToString(), true, is_mainte);
                }

                // Write_IdealPOS_Guest_File()  '<-- Ideal POS, to do
                await Task.Delay(1);
                return all_ok;
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
                return false;
            }

        }
        public void ApplyRateHousekeeping(int intRateID, long lngBookingID, long lngContactID, decimal dAccommNotDisbursed, decimal dCurrentOwnerBalance, int iTemplateID, bool bOnlyApplyOnce, bool bPassDates, out string sNewArrival, out string sNewDeparture, bool bPassGuestCount, int iNewGuestCount, int iNewInfantCount)
        {
            try
            {
                sNewArrival = "";
                sNewDeparture = "";
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
                sNewArrival = "";
                sNewDeparture = "";
            }
        }
        public void ApplyRateCharges(int intRateID, byte bteAppliesTo, long lngBookingID, long lngContactID, decimal dAccommNotDisbursed, decimal dCurrentOwnerBalance, bool bEarlyDisburse, bool bApplyNonPercentageCharges, bool bPassDates, out string sNewArrival, out string sNewDeparture, bool bPassGuestCount, int iNewGuestCount, bool bAccommNotSavedYet, bool bApplyPercentageCharges, int iNewInfantCount)
        {
            try
            {

                sNewArrival = "";
                sNewDeparture = "";
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
                sNewArrival = "";
                sNewDeparture = "";
            }
        }
        public void HideOriginalGuestCharges(int lngBookingID)
        {
            try
            {
                var sSQL = "UPDATE TBL_Accounts SET Deleted = 1, DeletedByID = 99997 WHERE BookingID = " + lngBookingID +
                           " AND Disbursed = 0 AND AppliedFromRate = 1 AND Type In(2,3);";
                var result = QExec(sSQL, a_connEncEzy);
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
            }
        }
        public void Recalc_Primary_Accomm_Amount(string a_accomm_amt, string a_roomid, string a_bookingid)
        {
            try
            {
                var sSQL = "";

                var bApplyGST = false;
                sSQL = "SELECT IncludeGST FROM TBL_AccountCodes WHERE ID = 28;";
                var sApplyGST = QExec(sSQL, a_connEncEzy);
                if (sApplyGST.Trim() == "")
                { bApplyGST = false; }
                else
                { bApplyGST = bool.Parse(sApplyGST); }

                var bOverrideGST = false;
                try
                {
                    sSQL = "SELECT OverrideGST FROM TBL_Property WHERE ID = " + a_roomid;
                    var sOverrideGST = QExec(sSQL, a_connEncEzy);
                    if (sOverrideGST.Trim() == "")
                    {
                        bOverrideGST = false;
                    }
                    else
                    {
                        bOverrideGST = bool.Parse(sOverrideGST);
                    }
                }
                catch (Exception aa1)
                {
                    var ss1 = aa1.Message;
                }

                ///////////////////
                if (bOverrideGST)
                {
                    sSQL = "SELECT GSTOnAccomm FROM TBL_Property WHERE ID = " + a_roomid;
                    sApplyGST = QExec(sSQL, a_connEncEzy);
                    if (sApplyGST.Trim() == "")
                    {
                        bApplyGST = false;
                    }
                    else
                    {
                        bApplyGST = bool.Parse(sApplyGST);
                    }
                }

                var dAmountInc = decimal.Parse(a_accomm_amt);
                decimal dAmountEx = 0.0M;
                decimal dAmountTax = 0.0M;

                if (bApplyGST)
                {
                    sSQL = "SELECT GST FROM TBL_BusinessDetails WHERE ID = 1;";
                    var sngGSTRate = double.Parse(QExec(sSQL, a_connEncEzy));
                    dAmountEx = dAmountInc / (1 + (decimal)sngGSTRate);
                    dAmountEx = Math.Round(dAmountEx, 2);
                    dAmountTax = dAmountInc - dAmountEx;
                }
                else
                {
                    dAmountEx = dAmountInc;
                    dAmountTax = 0;
                }

                sSQL = "UPDATE TBL_Accounts SET AmountEx = " + -dAmountEx + ", AmountTax = " + -dAmountTax + ", AmountInc = " + -dAmountInc
                       + " WHERE BookingID = " + a_bookingid + " AND Type = 1;";
                var result1 = QExec(sSQL, a_connEncEzy);
            }
            catch (Exception aa)
            {
                var ss = aa.Message;
            }
        }

        public bool Save_Booking_ThatMoved(string a_roomcode, string a_roomcodeName, string a_roomtypeid, string a_roomtypename, DateTime a_start, DateTime a_end, string a_id = "", bool a_resize = false, bool is_maintenance = false, bool is_dualkey = false)
        {
            try
            {
                var a_roomcode2 = a_roomcode.Split(',');
                var a_roomtypeid2 = a_roomtypeid.Split(',');
                var a_id2 = a_id.Split(',');
                if (is_dualkey)
                { //dual key here
                    for (int i = 0; i < 2; i++)
                    {
                        var a_sql = " Update TBL_Bookings set " + nl +
                                " roomcodeid = " + a_roomcode2[i] + ", ArrivalDateTime = '" + a_start.ToString("dd MMM yyyy HH:mm") + "', DepartureDateTime = '" + a_end.ToString("dd MMM yyyy HH:mm") + "'" + nl +
                                ", roomtypeid = " + a_roomtypeid2[i] + nl +
                                " Where ID = " + a_id2[i];

                        var a_result = QExec(a_sql, a_connEncEzy);
                        if (i == 0)
                            Update_TBL_AgentAccounts(a_roomcode2[i], a_id2[i]);

                        Record_Booking_Log(a_roomcode2[i], a_roomtypeid2[i], a_start, a_end, a_id2[i]);
                    }
                }
                else
                {
                    var a_sql = " Update TBL_Bookings set ";
                    a_sql += " roomcodeid = " + a_roomcode + ", ArrivalDateTime = '" + a_start.ToString("dd MMM yyyy HH:mm") + "', DepartureDateTime = '" + a_end.ToString("dd MMM yyyy HH:mm") + "'" + nl;
                    a_sql += ", roomtypeid = " + a_roomtypeid + nl;
                    a_sql += " Where ID = " + a_id;

                    var a_result = QExec(a_sql, a_connEncEzy);
                    Update_TBL_AgentAccounts(a_roomcode, a_id);

                    if (is_maintenance)
                    {
                        a_sql = " Update TBL_Bookings set ";
                        a_sql += " StartDate = '" + a_start.ToString("dd MMM yyyy HH:mm") + "', EndDate = '" + a_end.ToString("dd MMM yyyy HH:mm") + "'," + nl;
                        a_sql += " StartTime = '" + a_start.ToString("dd MMM yyyy HH:mm") + "', EndTime = '" + a_end.ToString("dd MMM yyyy HH:mm") + "'" + nl;
                        a_sql += " Where BookingID = " + a_id;
                        var a_result1 = QExec(a_sql, a_connEncEzy);
                    }
                    else
                    {
                        Record_Booking_Log(a_roomcode, a_roomtypeid, a_start, a_end, a_id);
                    }
                }
                //Write_IdealPOS_Guest_File()  '<-- Ideal POS
                return true;
            }
            catch (Exception q)
            {
                var w = q.Message;
                return false;
            }
        }
        public bool Save_Booking_ThatMoved2(string a_roomcode, string a_roomcode_linked, string a_roomtypeid, string a_roomtypename, DateTime a_start, DateTime a_end, string a_id = "", bool make_dualkey = false, bool is_maintenance = false, bool is_dualkey = false)
        {
            try
            {
                var a_roomcode2 = a_roomcode.Split(',');
                var a_roomtypeid2 = a_roomtypeid.Split(',');
                var a_id2 = a_id.Split(',');
                var a_roomcode_linked2 = a_roomcode_linked.Split(',');

                if (is_dualkey)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        var a_sql = " Update TBL_Bookings set " + nl +
                                " roomcodeid = " + a_roomcode2[i] + ", ArrivalDateTime = '" + a_start.ToString("dd MMM yyyy HH:mm") + "', DepartureDateTime = '" + a_end.ToString("dd MMM yyyy HH:mm") + "'" + nl +
                                ", roomtypeid = " + a_roomtypeid2[i] + nl + ", DualKeyRoomCode = " + a_roomcode_linked2[i];
                        if (make_dualkey)
                        {
                            a_sql += " , dualkey = 1,DualKeyAdditionalRecord =0 ";
                        }
                        a_sql += " Where ID = " + a_id2[i];

                        var a_result = QExec(a_sql, a_connEncEzy);

                        if (i == 0)
                            Update_TBL_AgentAccounts(a_roomcode2[i], a_id2[i]);

                        Record_Booking_Log(a_roomcode2[i], a_roomtypeid2[i], a_start, a_end, a_id2[i]);
                    }
                }

                //Write_IdealPOS_Guest_File()  '<-- Ideal POS
                return true;
            }
            catch (Exception q)
            {
                var w = q.Message;
                return false;
            }
        }

        public void Update_TBL_AgentAccounts(string a_new_roomcodeId, string a_bookingID)
        {
            try
            {
                var a_sql = " Update TBL_AgentAccounts set propertyID = " + a_new_roomcodeId;
                a_sql += " Where BookingID = " + a_bookingID;
                var a_result = QExec(a_sql, a_connEncEzy);
            }
            catch (Exception q)
            {
                var w = q.Message;
            }
        }
        public void Create_A_Booking_For_Dualkey_Dummy(string a_id, string a_roomcode, string a_roomtype, string a_linked_roomcode)
        {
            try
            {
                var a_sql = "";
                a_sql = "INSERT INTO TBL_Bookings(BookingNumber,MasterBookingID,ArrivalDateTime,DepartureDateTime," + nl +
                         " Status,BookingCreated,RoomTypeID,RoomCodeID,RequestRoom,DoNotMove," + nl +
                         " Adults,Children,Infants,AgentID,AgentBookingReference," + nl +
                         " RegionID,Source1ID,Source2ID,Source3ID,RateID,UseSeasonalRate,HideTariffOnRegoForm," + nl +
                         " TotalAccommodation,GuestRequestsComments,ManagersComments,HideGuestRequestsComments," + nl +
                         " Cancelled,Corporate,Maintenance,MaintenanceID,LongTermLease,Archive," + nl +
                         " Enquiry,DualKey,DualKeyRoomCode,DualKeyAdditionalRecord," + nl +
                         " InterfaceActivated,InterfaceDoNotDisturb,InterfaceMovies,InterfaceAdultMovies," + nl +
                         " POSBlockCharges,POSLimitCharges,POSChargeLimit,BookingChargeLimit," + nl +
                         " CardType,CardNumber,CardExpiry,CardCCV,CardName,PostCodeForAnalysis,DepositRequiredBy,DepositAmountRequired)" + nl +
                         " SELECT BookingNumber,MasterBookingID,ArrivalDateTime,DepartureDateTime," + nl +
                         "        Status,BookingCreated," + a_roomtype + "," + a_roomcode + ",RequestRoom,DoNotMove," + nl +
                         "        Adults,Children,Infants,AgentID,AgentBookingReference," + nl +
                         "        RegionID,Source1ID,Source2ID,Source3ID,RateID,UseSeasonalRate,HideTariffOnRegoForm," + nl +
                         "        TotalAccommodation,GuestRequestsComments,ManagersComments,HideGuestRequestsComments," + nl +
                         "        Cancelled,Corporate,Maintenance,MaintenanceID,LongTermLease,Archive," + nl +
                         "        Enquiry,1," + a_linked_roomcode + ",1," + nl + // 'DualKey,DualKeyRoomCode,DualKeyAdditionalRecord, 
                         "        InterfaceActivated,InterfaceDoNotDisturb,InterfaceMovies,InterfaceAdultMovies," + nl +
                         "        POSBlockCharges,POSLimitCharges,POSChargeLimit,BookingChargeLimit," + nl +
                         "        CardType,CardNumber,CardExpiry,CardCCV,CardName,PostCodeForAnalysis,DepositRequiredBy,DepositAmountRequired" + nl +
                         " FROM TBL_Bookings where id = " + a_id + nl +
                         "  ; SELECT SCOPE_IDENTITY() as ID ";
                var a_result = QExec(a_sql, a_connEncEzy);
                var iBookingID_JustCreated = QExec(a_sql, a_connEncEzy);
                a_sql = "INSERT INTO TBL_BookingGuestLink(BookingID,ContactID,FolioNumber ) select " + iBookingID_JustCreated + ",ContactID ,1 from  TBL_BookingGuestLink " +
                    " where BookingID = " + a_id;
                var result2 = QExec(a_sql, a_connEncEzy);
            }
            catch (Exception q)
            {
                var w = q.Message;
            }
        }
        public void Record_Booking_Log(string a_roomcode, string a_roomtypeid, DateTime a_start, DateTime a_end, string a_id)
        {
            try
            {
                var a_now_datetime = DateTime.Now.ToString("dd MMM yyyy HH:mm");
                var sSQL = "INSERT INTO TBL_BookingLog( UserID,BookingID,DateTimeRecorded," + nl +
                         " BookingNumber,MasterBookingID,ArrivalDateTime,DepartureDateTime," + nl +
                         " Status,BookingCreated,RoomTypeID,RoomCodeID,RequestRoom,DoNotMove," + nl +
                         " Adults,Children,Infants,AgentID,AgentBookingReference," + nl +
                         " RegionID,Source1ID,Source2ID,Source3ID,RateID,UseSeasonalRate,HideTariffOnRegoForm," + nl +
                         " TotalAccommodation,GuestRequestsComments,ManagersComments,HideGuestRequestsComments," + nl +
                         " Cancelled,Corporate,Maintenance,MaintenanceID,LongTermLease,Archive," + nl +
                         " Enquiry,DualKey,DualKeyRoomCode,DualKeyAdditionalRecord," + nl +
                         " InterfaceActivated,InterfaceDoNotDisturb,InterfaceMovies,InterfaceAdultMovies," + nl +
                         " POSBlockCharges,POSLimitCharges,POSChargeLimit,BookingChargeLimit," + nl +
                         " CardType,CardNumber,CardExpiry,CardCCV,CardName,PostCodeForAnalysis" + nl +
                         ")" + nl;
                sSQL += "select " + glngUserID + "," + a_id + ",'" + a_now_datetime + "'," + nl +
                         " BookingNumber,MasterBookingID,'" + a_start.ToString("dd MMM yyyy HH:mm") + "','" + a_end.ToString("dd MMM yyyy HH:mm") + "'," + nl +
                         " Status,BookingCreated," + a_roomtypeid + "," + a_roomcode + ",RequestRoom,DoNotMove," + nl +
                         " Adults,Children,Infants,AgentID,AgentBookingReference," + nl +
                         " RegionID,Source1ID,Source2ID,Source3ID,RateID,UseSeasonalRate,HideTariffOnRegoForm," + nl +
                         " TotalAccommodation,GuestRequestsComments,ManagersComments,HideGuestRequestsComments," + nl +
                         " Cancelled,Corporate,Maintenance,MaintenanceID,LongTermLease,Archive," + nl +
                         " Enquiry,DualKey,DualKeyRoomCode,DualKeyAdditionalRecord," + nl +
                         " InterfaceActivated,InterfaceDoNotDisturb,InterfaceMovies,InterfaceAdultMovies," + nl +
                         " POSBlockCharges,POSLimitCharges,POSChargeLimit,BookingChargeLimit," + nl +
                         " CardType,CardNumber,CardExpiry,CardCCV,CardName,PostCodeForAnalysis" + nl +
                         "  from tbl_bookings where id = " + a_id;
                var a_result = QExec(sSQL, a_connEncEzy);

                var a_contact_id = QExec("select top 1 ContactID from TBL_BookingGuestLink where FolioNumber =1 and BookingID = " + a_id, a_connEncEzy);
                if (int.Parse(a_contact_id) > 0)
                {
                    sSQL = "INSERT INTO TBL_ContactLog(UserID,ContactID,DateTimeRecorded," + nl +
                         " Reference, Surname, FirstName, Salutation, Company, FileAs, " + nl +
                         " PostalAddress1,PostalAddress2,Suburb,State,PostCode,Country," + nl +
                         " PhHome,PhWork,Fax,Mobile,Email,OtherText2,ContactCreated," + nl +
                         " CardType,CardNumber,CardExpiry,CardCCV,CardName,BlackList," + nl +
                         " PhysicalAddressLine1,PhysicalAddressLine2,PhysicalSuburb,PhysicalState,PhysicalPostCode,PhysicalCountry," + nl +
                         " Mobile2,Mobile3,Mobile4,SMSMobile1,SMSMobile2,SMSMobile3,SMSMobile4," + nl +
                         " Email2,Email3,Email4,Email2CC,Email3CC,Email4CC," + nl +
                         " NickName,Spouse,Children,Birthday,Anniversary,DoNotMarket,Inactive,ApplyGST,BondTransfer,Title,ReasonNotToMarket,BlacklistReason" + nl +
                         " )";
                    sSQL += "select " + glngUserID + "," + a_contact_id + ",'" + a_now_datetime + "'," + nl +
                         " Reference, Surname, FirstName, Salutation, Company, FileAs, " + nl +
                         " PostalAddress1,PostalAddress2,Suburb,State,PostCode,Country," + nl +
                         " PhHome,PhWork,Fax,Mobile,Email,OtherText2,ContactCreated," + nl +
                         " CardType,CardNumber,CardExpiry,CardCCV,CardName,BlackList," + nl +
                         " PhysicalAddressLine1,PhysicalAddressLine2,PhysicalSuburb,PhysicalState,PhysicalPostCode,PhysicalCountry," + nl +
                         " Mobile2,Mobile3,Mobile4,SMSMobile1,SMSMobile2,SMSMobile3,SMSMobile4," + nl +
                         " Email2,Email3,Email4,Email2CC,Email3CC,Email4CC," + nl +
                         " NickName,Spouse,Children,Birthday,Anniversary,DoNotMarket,Inactive,ApplyGST,BondTransfer,Title,ReasonNotToMarket,BlacklistReason" + nl +
                         "  from TBL_Contacts where id = " + a_contact_id;

                    var a_result2 = QExec(sSQL, a_connEncEzy);
                }
                else
                {
                    // error message
                }
            }
            catch (Exception q)
            {
                var w = q.Message;
            }
        }

        public static string QExec(string a_sql, string conn_str)
        {

            try
            {
                var result = "";
                using (var con = new SqlConnection(conn_str))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand(a_sql, con))
                    {
                        var a_result = command.ExecuteScalar();
                        if (a_result != null)
                        {
                            result = a_result.ToString();
                        }
                        else
                        {
                            result = "";
                        }
                    }
                }
                return result;
            }
            catch (Exception dd)
            {
                var ss = "error " + dd.Message;
                return ss;
                // MessageBox.Show(ss);
                //throw;
            }
        }
        public static List<string> QExecList(string a_sql, string conn_str, int numElement = 1)
        {
            var resultList = new List<string>();
            try
            {
                using (var con = new SqlConnection(conn_str))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand(a_sql, con))
                    {
                        SqlDataReader sqlReader = command.ExecuteReader();
                        while (sqlReader.Read())
                        {
                            for (int i = 0; i < numElement; i++)
                            {
                                resultList.Add(sqlReader.GetValue(i).ToString());
                            }
                        }
                        sqlReader.Close();
                    }
                }
                return resultList;
            }
            catch (Exception dd)
            {
                var ss = "error " + dd.Message;
                return resultList;
            }
        }

        public static TimeZoneInfo GetTimeZoneInfo(string region)
        {
            switch (region)
            {
                case "Queensland":
                    return TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
                case "New South Wales":
                case "ACT":
                case "Victoria":
                    return TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                case "Tasmania":
                    return TimeZoneInfo.FindSystemTimeZoneById("Tasmania Standard Time");
                case "South Australia":
                    return TimeZoneInfo.FindSystemTimeZoneById("Cen. Australia Standard Time");
                case "Western Australia":
                    return TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
                case "Northern Territory":
                    return TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                case "New Zealand":
                    return TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                default:
                    return TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
            }
        }
        public static DateTime ToReiLocal(DateTime aDateTime, TimeZoneInfo timezoneInfo)
        {
            if (timezoneInfo == null) throw new ArgumentNullException(nameof(timezoneInfo));
            // Convert to local date time based on TimezoneInfo
            aDateTime = TimeZoneInfo.ConvertTime(aDateTime, timezoneInfo);

            // Is daylight saving on?
            return timezoneInfo.IsDaylightSavingTime(aDateTime) ? aDateTime.AddHours(1) : aDateTime;
        }
        public static DateTime ToReiLocal2(DateTime aDateTime)
        {
            var timezoneInfo = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
            // Convert to local date time based on TimezoneInfo
            aDateTime = TimeZoneInfo.ConvertTime(aDateTime, timezoneInfo);

            // Is daylight saving on?
            return timezoneInfo.IsDaylightSavingTime(aDateTime) ? aDateTime.AddHours(1) : aDateTime;
        }
        protected static string ConvertToHex(string GridColour)
        {
            if (!string.IsNullOrEmpty(GridColour))
            {
                if (GridColour != "-1")
                {
                    Color c = ColorTranslator.FromWin32(Convert.ToInt32(GridColour));
                    try
                    {
                        return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
                    }
                    catch (Exception ex)
                    {
                        var ss = ex.Message;
                        return "#FFFFF";
                    }
                }
            }
            return "#FFFFF";
        }
        public async Task FindResponsiveness()
        {
            var mobile = await JSRuntime.InvokeAsync<bool>("isDevice");
            var isDevice = mobile ? "Mobile" : "Desktop";

        }
        public async Task NavTo1111(string url, bool isNewTab)
        {
            if (isNewTab)
            {
                await JSRuntime.InvokeAsync<object>("open", url, "_blank");
            }
            else
            {
                // NavManager.NavigateTo(url);
            }
        }

       

        //public static List<BookingData> GenerateBookings()
        //{
        //    try
        //    {
        //        DateTime date = new DateTime(2018, 4, 1);
        //        List<BookingData> data = new List<BookingData>(3600);
        //        var id = 1;
        //        for (var i = 0; i < 300; i++)
        //        {
        //            Random random = new Random();
        //            List<int> listNumbers = new List<int>();
        //            int[] randomCollection = new int[24];
        //            int number;
        //            int max = 30;
        //            for (int a = 0; a < 12; a++)
        //            {
        //                do
        //                {
        //                    number = random.Next(max);
        //                } while (listNumbers.Contains(number));
        //                listNumbers.Add(number);
        //                var startDate = date.AddDays(number);
        //                startDate = startDate.AddMilliseconds((((number % 10) * 10) * (1000 * 60)));
        //                var endDate = startDate.AddMilliseconds(((1440 + 30) * (1000 * 60)));
        //                data.Add(new BookingData
        //                {
        //                    Id = id,
        //                    Subject = "Event #" + id,
        //                    StartTime = startDate,
        //                    EndTime = endDate,
        //                    // IsAllDay = (id % 10 == 0) ? false : true,
        //                    RoomCodeId = (i % 299),
        //                    RoomTypeId = (i % 10)
        //                });
        //                id++;
        //            }
        //        }
        //        return data;
        //    }
        //    catch (Exception rr)
        //    {
        //        var ss = rr.Message;
        //        return new List<BookingData>();
        //        // throw;
        //    }

        //}

        //    public List<RoomTypeAndRoomData> RoomTypeData { get; set; } = new List<RoomTypeAndRoomData>
        //{
        //    new RoomTypeAndRoomData{ RoomTypeText = "1 Bed Room", Id = 1, RoomTypeColor = "#cb6bb2" },
        //    new RoomTypeAndRoomData{ RoomTypeText = "2 Bed Room", Id = 2, RoomTypeColor = "#56ca85" }
        //};
        //    public List<RoomTypeAndRoomData> RoomCodeData { get; set; } = new List<RoomTypeAndRoomData>
        //{
        //    new RoomTypeAndRoomData{ RoomCodeText = "U101", Id = 1, RoomTypeGroupId = 1, RoomCodeColor = "#ffaa00" },
        //    new RoomTypeAndRoomData{ RoomCodeText = "U102", Id = 2, RoomTypeGroupId = 2, RoomCodeColor = "#f8a398" },
        //    new RoomTypeAndRoomData{ RoomCodeText = "U103", Id = 3, RoomTypeGroupId = 1, RoomCodeColor = "#7499e1" }
        //};
        //    public List<BookingData> DataSource = new List<BookingData>
        //{
        //    new BookingData { Id = 1, Subject = "Booking", StartTime = new DateTime(2020, 2, 19, 9, 30, 0) , EndTime = new DateTime(2020, 2, 19, 11, 0, 0), RoomCodeId = 1, RoomTypeId = 1 }
        //};
        public class BookingData
        {
            public int Id { get; set; }
            public string Subject { get; set; }
            public string Location { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public string Description { get; set; }
            //public bool IsAllDay { get; set; }
            //public string RecurrenceRule { get; set; }
            //public string RecurrenceException { get; set; }
            //public Nullable<int> RecurrenceID { get; set; }
            public int RoomCodeId { get; set; }
            public int RoomTypeId { get; set; }
            public bool IsReadonly { get; set; }

            public string ElementType { get; set; }
            public string BookingBarColor { get; set; }
            public string BookingBarText { get; set; }
            public string GuestName { get; set; }
            public string Pax { get; set; }
            public int Status { get; set; }
            public string StatusText { get; set; }
            public string RoomCodeText { get; set; }
            public string RoomTypeText { get; set; }
            public string Mobile { get; set; }
            public string Comments { get; set; }
            public int BookingNumber { get; set; }
            public string BookingDates { get; set; }
            public bool DualKeyDummy { get; set; }
            public bool DualKey { get; set; }
            public int DualKeyLinkedRoomID { get; set; }
            public string DualKeyLinkedRoomCodeName { get; set; }
            public DateTime StartTimeOriginal { get; set; }
            public DateTime EndTimeOriginal { get; set; }
            public int RoomCodeIdOriginal { get; set; }
            public int RoomTypeIdOriginal { get; set; }
            public virtual DateTime StartTimeValue { get; set; }
            public virtual DateTime EndTimeValue { get; set; }
            public virtual RoomTypeAndRoomData ResourceData { get; set; }

        }
        public class RoomTypeAndRoomData
        {
            public int Id { get; set; }  // **** IMPORTANAT -> this can be roomcodeid and roomtypeid depening on if this is a room list or a roomtype list
            public string RoomTypeText { get; set; }
            public string RoomTypeColor { get; set; }
            public string RoomCodeText { get; set; }
            public string RoomCodeColor { get; set; }
            public string RoomCodeForeColor { get; set; }
            public int RoomTypeGroupId { get; set; }

            public int RoomListRoomType_Id { get; set; }  // this is used for a room list  only > room type id
            public string RoomGSTInfo { get; set; }
            public bool RoomOnlineBookable { get; set; }
            public bool DualKey { get; set; }
            public int DualKeyLinkedRoomID { get; set; }
            public int DualKeyResultRoomTypeID { get; set; }
            public string DualKeyLinkedRoomCodeName { get; set; }

            public bool Expand { get; set; }

        }

        public class WeekOfYear
        {
            public int ID { get; set; }
            public string Text { get; set; }
        }
        public class BookingParameter
        {
            public int WindowHeight { get; set; }
            public int WindowWidth { get; set; }
            public string Username { get; set; }
            public int BooingId { get; set; }
            public string ClientTimeZoneName { get; set; }
            public string GuestName { get; set; }
            public string WidowOpenDateTime { get; set; }
        }
        public static class BookingGridCellSelected
        {
            public static int RoomCodeId { get; set; }
            public static string RoomCodeText { get; set; }
            public static string EndDate { get; set; }
            public static string StartDate { get; set; }
            public static string Nights { get; set; }
        }
        public class BookingSidePanelLocalData
        {
            public List<CurrentlyOpenWindows> CurrrentWindowList { get; set; } = new List<CurrentlyOpenWindows>();
            public List<RecentlyOpenBookings> RecentBookingList { get; set; } = new List<RecentlyOpenBookings>();
        }
        public class CurrentlyOpenWindows
        {
            public string WindowId { get; set; }
            public string WindowType { get; set; } //add new booking, edit booking, search, allotment, room search,
            public string WindowTitle { get; set; } //
            public DateTime DateOpened { get; set; }
            public int BookingId { get; set; }
            public int BookingNumber { get; set; }
            public string GuestName { get; set; }

        }
        public class RecentlyOpenBookings
        {
            public DateTime DateOpened { get; set; }
            public int BookingId { get; set; }
            public int BookingNumber { get; set; }
            public string GuestName { get; set; }
        }

        protected string pPhoneMenu1 = "    <div class='col-sm-6'>  " +
            "       <table width='100%'>                                                                                                  " +
            "          <tr>                                                                                                                             " +
            "              <td><SfMenu @ref = 'myMenu1' Items='@menuItems1' CssClass='e-rounded-menu1'></SfMenu> </td>                                  " +
            "              <td><SfMenu Items = '@menuItems2' CssClass='e-rounded-menu'></SfMenu></td>                                                   " +
            "              <td><SfMenu Items = '@menuItems3' CssClass='e-rounded-menu'></SfMenu></td>                                                   " +
            "              <td><SfMenu Items = '@menuItems4' CssClass='e-rounded-menu'></SfMenu> </td>                                                  " +
            "              <td><SfMenu Items = '@menuItems5' CssClass='e-rounded-menu'></SfMenu></td>                                                   " +
            "              <td><SfMenu Items = '@menuItems6' CssClass='e-rounded-menu'></SfMenu> </td>                                                  " +
            "          </tr>                                                                                                                            " +
            "      </table>                                                                                                                             " +
            "  </div>                                                                                                                                   " +
            "  <div class='col-sm-6'>                                                                                                                   " +
            "      <table width = '100%' >                                                                                                              " +
            "          < tr >                                                                                                                           " +
            "              < td >< SfMenu Items='@menuItems7' CssClass='e-rounded-menu1'></SfMenu> </td>                                                " +
            "              <td><SfMenu Items = '@menuItems8' CssClass='e-rounded-menu1'></SfMenu> </td>                                                 " +
            "              <td>                                                                                                                         " +
            "                  <SfMenu Items = '@menuItems9' CssClass='e-rounded-menu1'>                                                                " +
            "                      <MenuEvents ItemSelected = 'itemSelected' ></ MenuEvents >                                                           " +
            "                  </ SfMenu >                                                                                                              " +
            "              </ td >                                                                                                                      " +
            "              @if(pWindowWidth > 1100)                                                                                                     " +
            "              {                                                                                                                            " +
            "                  <td><button class='btn btn-info btn-sm' @onclick='GoDate1'><i class='fa fa-soundcloud'></i></button></td>                " +
            "                  <td style = 'text-align :right ' >< button class='btn btn-info btn-sm'><i class='fa fa-youtube-play'></i></button> </td> " +
            "              }                                                                                                                            " +
            "          </tr>                                                                                                                            " +
            "      </table>                                                                                                                             " +
            "  </div>";

        protected string pPhoneMenu2 = "   " +
            " <div class='col-sm-6'> " +
     "   < table width='100%'>                                                                                                                                                                               " +
  "          <tr>                                                                                                                                                                                            " +
  "              @if(pWindowWidth > 1100)                                                                                                                                                                    " +
  "      {                                                                                                                                                                                                   " +
  "                  < td width = '20px' > Showing </ td >                                                                                                                                                   " +
  "              }                                                                                                                                                                                           " +
  "              <td width = '190px' > < SfDatePicker TValue='DateTime?' @bind-Value='@myDatePickerDate' Format='dd/MM/yyyy' Width='180px'></SfDatePicker></td>                                              " +
  "              <td><button class='btn btn-primary btn-sm' @onclick='GoToday'><i class='fa fa-reply'></i></button></td>                                                                                     " +
  "              <td width = '20px' > @pGridPeriodText </ td >                                                                                                                                               " +
  "              < td > < SfNumericTextBox TValue='int?' Width='95px' Format='###' Max=@pGridPeriodMax Min = @pGridPeriodMin Step=@pGridPeriodStep @bind-Value='@pGridPeriodValues'></SfNumericTextBox></td> " +
  "              @*<td><button class='btn btn-success btn-sm' disabled='@isLoading' @onclick='RefreshBookingGrid'>Refresh</button></td>*@                                                                    " +
  "          </tr>                                                                                                                                                                                           " +
  "      </table>                                                                                                                                                                                            " +
  "  </div>                                                                                                                                                                                                  " +
  "  <div class='col-sm-6'>                                                                                                                                                                                  " +
  "      <table width = '100%' >                                                                                                                                                                             " +
  "          < tr >                                                                                                                                                                                          " +
  "              < td >< button class='btn btn-success btn-sm' @onclick='RefreshBookingGrid'>Refresh</button></td>                                                                                           " +
  "              @* width 1100 is ipad pro *@                                                                                                                                                                " +
  "              @if(pWindowWidth > 1100)                                                                                                                                                                    " +
  "      {                                                                                                                                                                                                   " +
  "                  < td width = '20px' > Week / Year </ td >                                                                                                                                               " +
  "                                                                                                                                                                                                          " +
  "                   < td >                                                                                                                                                                                 " +
  "                                                                                                                                                                                                          " +
  "                       < select class='form-control' style=' width: 85px;height: 31px;font-size: 13px;padding: 1px 2px 2px 2px;'>                                                                         " +
  "                          @foreach(var country in WeekOfYearData)                                                                                                                                         " +
  "      {                                                                                                                                                                                                   " +
  "                              < option value = '@country.ID' > @country.Text </ option >                                                                                                                  " +
  "                          }                                                                                                                                                                               " +
  "                      </select>                                                                                                                                                                           " +
  "                  </td>                                                                                                                                                                                   " +
  "              }                                                                                                                                                                                           " +
  "              <td>@WeekOfYearText</td>                                                                                                                                                                    " +
  "                                                                                                                                                                                                          " +
  "              <td width = '20px' > Filters </ td >                                                                                                                                                        " +
  "              < td >                                                                                                                                                                                      " +
  "                  < SfMultiSelect @ref='RoomTypeDropdownObj' TValue='string[]' Width='180px' PopupWidth='350px' Placeholder='RoomType' ShowSelectAll=true SelectAllText='Select All' UnSelectAllText='unSelect All' Mode='VisualMode.CheckBox' DataSource='@RoomTypeDropdownList'>                                  " +
  "                      <MultiSelectFieldSettings Text = 'Text' Value='ID'></MultiSelectFieldSettings>                                                                                                                                                                                                                " +
  "                  </SfMultiSelect>                                                                                                                                                                                                                                                                                  " +
  "              </td>                                                                                                                                                                                                                                                                                                 " +
  "              @if(pWindowWidth > 600)                                                                                                                                                                                                                                                                               " +
  "  {                                                                                                                                                                                                                                                                                                                 " +
  "                  < td style = 'width :100px' >                                                                                                                                                                                                                                                                     " +
  "                                                                                                                                                                                                                                                                                                                    " +
  "                       < SfMultiSelect @ref = 'BookingTypeDropdownObj' TValue = 'string[]' Width = '100px' PopupWidth = '200px' Placeholder = 'BookingType' ShowSelectAll = false MaximumSelectionLength = 1 UnSelectAllText = 'unSelect All' Mode = 'VisualMode.CheckBox' DataSource = '@BookingTypeDropdownList' >" +
  "                                                                                                                                                                                                                                                                                                                    " +
  "                                              < MultiSelectFieldSettings Text = 'Text' Value = 'ID' ></ MultiSelectFieldSettings >                                                                                                                                                                                  " +
  "                                                                                                                                                                                                                                                                                                                    " +
  "                                             </ SfMultiSelect >                                                                                                                                                                                                                                                     " +
  "                                                                                                                                                                                                                                                                                                                    " +
  "                                         </ td >                                                                                                                                                                                                                                                                    " +
  "                                                                                                                                                                                                                                                                                                                    " +
  "                                                                                                                                                                                                                                                                                                                    " +
  "                                         < td style = 'width :100px' >                                                                                                                                                                                                                                              " +
  "                                                                                                                                                                                                                                                                                                                    " +
  "                                              < SfMultiSelect @ref = 'AgentTypeDropdownObj' TValue = 'string[]' Width = '100px' PopupWidth = '280px' Placeholder = 'Agent' ShowSelectAll = true SelectAllText = 'Select All' UnSelectAllText = 'unSelect All' Mode = 'VisualMode.CheckBox' DataSource = '@AgentDropdownList' >" +
  "                                                                                                                                                                                                                                                                                                                              " +
  "                                                                     < MultiSelectFieldSettings Text = 'Text' Value = 'ID' ></ MultiSelectFieldSettings >                                                                                                                                                                     " +
  "                                                                                                                                                                                                                                                                                                                              " +
  "                                                                    </ SfMultiSelect >                                                                                                                                                                                                                                        " +
  "                                                                                                                                                                                                                                                                                                                              " +
  "                                                                </ td >                                                                                                                                                                                                                                                       " +
  "              }                                                                                                                                                                                                                                                                                                               " +
  "  @if(pWindowWidth > 600)                                                                                                                                                                                                                                                                                                     " +
  "  {                                                                                                                                                                                                                                                                                                                           " +
  "                  < td >                                                                                                                                                                                                                                                                                                      " +
  "                      @pShowing                                                                                                                                                                                                                                                                                               " +
  "                  </ td >                                                                                                                                                                                                                                                                                                     " +
  "              }                                                                                                                                                                                                                                                                                                               " +
  "          </tr>                                                                                                                                                                                                                                                                                                               " +
  "      </table>                                                                                                                                                                                                                                                                                                                " +
  "  </div>                                                                                                                                                                                     " +
  "          ";






        //{
        //    a_id_list = a_id_list.TrimEnd(',');

        //    var sqlStr = "update TBL_PageViews set ProviderNotified = 1, DateNotified=getdate(), PageViewed = PageViewed + '-' where id in ( " + a_id_list + ") ";
        //     sqlStr = ""
        //    // comment below <<<<<<<<<<<-*****************************
        //    //sqlStr = "update TBL_PageViews set ProviderNotified = 1, DateNotified=getdate(), PageViewed = PageViewed + ' -' where id in (2474593, 2474590)";
        //    using (SqlCommand cmd = new SqlCommand(sqlStr, conn))
        //    {
        //        //cmd.Parameters.AddWithValue("@a_id_list", "2474593,2474590");
        //        //cmd.Parameters.AddWithValue("@Address", "Kerala");
        //        int rows = cmd.ExecuteNonQuery();
        //        rows = rows + 0;
        //        //rows number of record got updated
        //    }
        //}


    }
}
