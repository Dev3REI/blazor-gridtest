using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Syncfusion.Blazor;
using Syncfusion.Licensing;
using Telerik.Reporting.Cache.File;
using Telerik.Reporting.Services;
using Telerik.Reporting.Services.AspNetCore;
using WebApplication1.Pages.BookingServices;

using Microsoft.AspNetCore.HttpsPolicy;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using System.Threading;
using Microsoft.AspNetCore.Components.Server.Circuits;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            // SyncfusionLicenseProvider.RegisterLicense("MjQ2NzkzQDMxMzgyZTMxMmUzMEtxVDdmbnh6RTNVWGlNSVpoaW1NOXpsUlo3OXVlUnN5YkR4Y1hxaGx2c1k9
            SyncfusionLicenseProvider.RegisterLicense("Mjg5NjY3QDMxMzgyZTMyMmUzMFUydVJQYVB3OVhadGV3MTR1dndCUk4xM3FOY1ZUYWdzZS94R2Y1dHJnUE09");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddControllers();
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.AddRazorPages()
             .AddNewtonsoftJson();
            // Configure dependencies for ReportsController.
            services.TryAddSingleton<IReportServiceConfiguration>(sp =>
                new ReportServiceConfiguration
                {
                    ReportingEngineConfiguration = ConfigurationHelper.ResolveConfiguration(sp.GetService<IWebHostEnvironment>()),
                    HostAppId = "ReportingCore3App",
                    Storage = new FileStorage(),
                    ReportResolver = new ReportFileResolver(
                        System.IO.Path.Combine(sp.GetService<IWebHostEnvironment>().ContentRootPath, "Reports"))
                });

            services.AddServerSideBlazor();

            //services.AddSignalR().AddAzureSignalR(options =>
            //{
            //    options.ServerStickyMode =
            //        Microsoft.Azure.SignalR.ServerStickyMode.Required;
            //    options.ConnectionCount = 10;
            //    options.ConnectionString = "Endpoint=https://reimaster.service.signalr.net;AccessKey=LFUbfTHCNIbkCIDmEdzCVJBC3lhYMBPIIwtPfwYRLHw=;Version=1.0;";
            //});
            // from synfusion
            services.AddSyncfusionBlazor();  
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            // below from synfusion
            services.AddServerSideBlazor().AddCircuitOptions(options => { options.DetailedErrors = true; });
            services.AddServerSideBlazor().AddHubOptions(o =>
            {
                o.MaximumReceiveMessageSize = 102400000;
            });
            // from synfusion

            services.AddHttpContextAccessor();
            services.AddScoped<HttpContextAccessor>();

            services.AddBlazoredLocalStorage();
            // services.TryAddSingleton< BService >();
            services.AddSingleton<CircuitHandler, TrackingCircuitHandler>();

            services.AddSingleton<BookingModuleData>();

            services.AddScoped<BrowserService>(); // scoped service
            
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.OnAppendCookie = cookieContext =>
                  CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                options.OnDeleteCookie = cookieContext =>
                  CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Cookie.SameSite = SameSiteMode.None;
                    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                    options.Cookie.IsEssential = true;
                });

            services.AddSession(options =>
            {
                options.Cookie.SameSite = SameSiteMode.None;
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.Cookie.IsEssential = true;
            });
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCookiePolicy();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Booking}/{action=Index}/{id?}");
                endpoints.MapFallbackToController("Blazor", "Booking");
                endpoints.MapBlazorHub();

            });
            app.UseWebSockets();

            var cc = "en-AU"; //Configuration.GetValue<string>("AppSettings:CurrentCulture");

            var supportedCultures = new[]{
                new CultureInfo(cc)
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(cc),
                SupportedCultures = supportedCultures,
                FallBackToParentCultures = false
            });
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CreateSpecificCulture(cc);
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.CreateSpecificCulture(cc);
        }


        private void CheckSameSite(HttpContext httpContext, CookieOptions options)
        {
            if (options.SameSite == SameSiteMode.None)
            {
                var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
                if (SameSite.BrowserDetection.DisallowsSameSiteNone(userAgent))
                {
                    options.SameSite = SameSiteMode.Unspecified;
                }
            }
        }
        public class TrackingCircuitHandler : CircuitHandler
        {
            private HashSet<Circuit> circuits = new HashSet<Circuit>();

            public override Task OnConnectionUpAsync(Circuit circuit,
                CancellationToken cancellationToken)
            {
                circuits.Add(circuit);

                return Task.CompletedTask;
            }

            public override Task OnConnectionDownAsync(Circuit circuit,
                CancellationToken cancellationToken)
            {
                circuits.Remove(circuit);

                return Task.CompletedTask;
            }

            public int ConnectedCircuits => circuits.Count;
        }
    }
}
