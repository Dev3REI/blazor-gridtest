﻿

using Microsoft.AspNetCore.Components;
using Microsoft.VisualBasic;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Navigations;
using Syncfusion.Blazor.Popups;
using Syncfusion.Blazor.Schedule;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Pages.BookingServices;
using static WebApplication1.Pages.Bookings.BookingGridBase;

namespace WebApplication1.Pages.Bookings
{
    public class BookingDetailBase : ComponentBase
    {
        public static string glngUserID = "4";
        public static string a_connEncEzy = MyCrypt.EzyConnStr();
        static string nl = System.Environment.NewLine;
        public static string pGridPeriodText { get; set; } = "Months"; // or Days
        public static int? pGridPeriodValues { get; set; } = 2; // default value -> 2 Months;  //  or aDays 60 (about 9 weeks)
        public static int pGridPeriodMax { get; set; } = 13; // or 400
        public static int pGridPeriodMin { get; set; } = 1;  // or 10
        public static int pGridPeriodStep { get; set; } = 1; // or 5
        public static string gstrVersion = "";  //Z for readonly

      
        [Parameter]
        public BookingParameter myPara { get; set; }
        [Parameter]
        public string Bookingid { get; set; }
        [Parameter]
        public string Guestname { get; set; }

        protected override void OnParametersSet()
        {
            try
            {

                var a_now = DateTime.Now.ToString();

                Guestname = myPara.GuestName;
                Bookingid = myPara.BooingId.ToString();
            //    pClientTimeZone = GetTimeZoneInfo(myPara.ClientTimeZoneName);

                //pBookingGuestName = pguestname;
                //pBookingId = pbookingid;

            }
            catch (Exception dd)
            {
                var qq = dd.Message;
                // throw;
            }
        }

    }
}
