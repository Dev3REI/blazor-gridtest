﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace REIMaster.Interfaces.Models.Bookings
{
    public class EmployeeDetails
    {
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        public bool Gender { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        public string PhoneNumber { get; set; }
        [Required]
        public DateTime? DOB { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [Range(0, 20, ErrorMessage = "The Experience range should be 0 to 20")]
        public decimal? TotalExperience { get; set; }
    }
    public class BookingBarDetails
    {
        public BookingBarDetails(string statusText, string color)
        {
            StatusText = statusText;
            BKColor = color?.ToLower();
        }
        public BookingBarDetails WithText(string barText)
        {
            BarText = barText;
            return this;
        }
        public string BKColor { get; set; }
        public string StatusText { get; set; }
        public string BarText { get; set; }
    }
    public class BookingData
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Description { get; set; }
        //public bool IsAllDay { get; set; }
        //public string RecurrenceRule { get; set; }
        //public string RecurrenceException { get; set; }
        //public Nullable<int> RecurrenceID { get; set; }
        public int RoomCodeId { get; set; }
        public int RoomTypeId { get; set; }
        public bool IsReadonly { get; set; }

        public string ElementType { get; set; }
        public BookingBarDetails BookingBarDetails { get; set; }
        public string GuestName { get; set; }
        public string Pax { get; set; }
        public int Status { get; set; }
        public string RoomCodeText { get; set; }
        public string RoomTypeText { get; set; }
        public string Mobile { get; set; }
        public string Comments { get; set; }
        public int BookingNumber { get; set; }
        public string BookingDates { get; set; }
        public bool DualKeyDummy { get; set; }
        public bool DualKey { get; set; }
        public int DualKeyLinkedRoomID { get; set; }
        public string DualKeyLinkedRoomCodeName { get; set; }
        public DateTime StartTimeOriginal { get; set; }
        public DateTime EndTimeOriginal { get; set; }
        public int RoomCodeIdOriginal { get; set; }
        public int RoomTypeIdOriginal { get; set; }
        public bool BlackList { get; set; }
        public virtual DateTime StartTimeValue { get; set; }
        public virtual DateTime EndTimeValue { get; set; }
        public virtual RoomTypeAndRoomData ResourceData { get; set; }

    }
    public class RoomTypeAndRoomData
    {
        public int Id { get; set; }  // **** IMPORTANT -> this can be roomcodeid and roomtypeid depending on if this is a room list or a roomtype list
        public string RoomTypeText { get; set; }
        public string RoomTypeColor { get; set; }
        public string RoomCodeText { get; set; }
        public string RoomCodeColor { get; set; }
        public string RoomCodeForeColor { get; set; }
        public int RoomTypeGroupId { get; set; }

        public int RoomListRoomType_Id { get; set; }  // this is used for a room list  only > room type id
        public string RoomGSTInfo { get; set; }
        public bool RoomOnlineBookable { get; set; }
        public bool DualKey { get; set; }
        public int DualKeyLinkedRoomID { get; set; }
        public int DualKeyResultRoomTypeID { get; set; }
        public string DualKeyLinkedRoomCodeName { get; set; }

        public bool Expand { get; set; }
    }
    public class BookingUserSettings
    {
        public int Id { get; set; }
        public string gstrBookingStockDesc { get; set; }
        public bool gUseDescOnBooking { get; set; }
        public bool gUseBookingNetAccom { get; set; }
    }
    public class DropDownData
    {
        public int ID { get; set; }
        public string Text { get; set; }
    }
}
