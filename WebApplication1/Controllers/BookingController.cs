﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using static WebApplication1.Pages.Bookings.BookingGridBase;

namespace WebApplication1.Controllers
{
    public class BookingController : Controller
    {
        
        public IActionResult Index()
        {
            var bookingModel = new BookingListTypeViewModel();
            bookingModel.Caption = "Booking Grid Test";
            bookingModel.UserID = 7;

            return View("/Views/Home/Privacy.cshtml");
            //  return View("/Pages/Logins/Login.cshtml");
           // return View("/Pages/Bookings/BookingGridView.cshtml", bookingModel);

        }
        [Route("/booking/bookinggrid/{a_height}/{a_width}/{a_time}/{a_time2}")]
        public IActionResult BookingGrid(int a_height, int a_width, string a_time, string a_time2)
        {
            //var myTimezone = GetTimeZoneInfo("Queensland");
            var qq = a_time; // Hmmss
            var bookingModel = new BookingParameter();
            bookingModel.Username = "Booking Grid Test";
            bookingModel.WindowHeight = a_height;
            bookingModel.WindowWidth = a_width;
            bookingModel.ClientTimeZoneName = "Queensland";
            bookingModel.WidowOpenDateTime = a_time + ' ' + DateTime.UtcNow.ToString("H:mm:ss");

            return View("/Pages/Bookings/BookingGridView.cshtml", bookingModel);
        }
        [Route("/booking/bookingdetail/{bookingid}/{guestname}")]
        public IActionResult BookingDetail(int bookingid, string guestname)
        {
            var bookingModel = new BookingParameter();
            bookingModel.Username = "Booking Main Page";
            bookingModel.WindowHeight = 0;
            bookingModel.WindowWidth = 0;
            bookingModel.BooingId = bookingid;
            bookingModel.GuestName = guestname;
            return View("/Pages/Bookings/BookingDetailView.cshtml", bookingModel);
        }


        //[Route("/booking/bookings/{bookingid}/{guestname}")]
        //public IActionResult Bookings(int bookingid, string guestname)
        //{
        //    var bookingModel = new BookingParameter();
        //    bookingModel.Username = "Booking Grid Test";
        //    bookingModel.WindowHeight = 0;
        //    bookingModel.WindowWidth = 0;
        //    bookingModel.BooingId = bookingid;
        //    bookingModel.GuestName = guestname;
        //    return View("/Pages/Bookings/BookingsView.cshtml", bookingModel);
        //}
        [Route("/booking/bookingmainpage/{bookingid}/{guestname}")]
        public IActionResult BookingMainPage(int bookingid, string guestname)
        {
            var bookingModel = new BookingParameter();
            bookingModel.Username = "Booking Main Page";
            bookingModel.WindowHeight = 0;
            bookingModel.WindowWidth = 0;
            bookingModel.BooingId = bookingid;
            bookingModel.GuestName = guestname;
            return View("/Pages/Bookings/BookingMainPageView.cshtml", bookingModel);
        }


        [Route("/booking/TestGrid/{a_height}/{a_width}/{a_time}")]
        public IActionResult TestGrid(int a_height, int a_width,string a_time)
        {
            var qq = a_time;
            var bookingModel = new BookingParameter();
            bookingModel.Username = "Booking Grid Test";
            bookingModel.WindowHeight = a_height;
            bookingModel.WindowWidth = a_width;
            bookingModel.ClientTimeZoneName = "Queensland";
            return View("/Pages/Bookings/TestGridView.cshtml", bookingModel);
        }


        [Route("/booking/test1/")]
        public IActionResult Test1()
        {
            return View("/Pages/Bookings/Test1.cshtml" );
        }

        public IActionResult Blazor()
        {
            return View("_Host");
        }


    }
}