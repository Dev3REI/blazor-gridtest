﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class BookingListTypeViewModel
    {
        public int? Status { get; set; }
        public IEnumerable<string> UserDefinedLists { get; set; }
        public int UserID { get; set; }
        public bool Inactive { get; set; }
        public string Caption { get; set; }
    }
}
