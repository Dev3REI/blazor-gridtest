﻿
function keepalive(token) {
    var a_ok = false;
    alert(token);
    $.ajax({
        url: '/user/keepaliveuser',
        type: 'post',
        data: null,
        headers: {
            'Cookie': '.AspNetCore.Identity.Application=' + token,
        },
        dataType: 'json',
        success: function (data) {
            alert(data);
        }
    });
}

function alert100() {
    alert('100');
}

function openSidePanel() {
    try {

        document.getElementById("mySidepanel").style.width = "480px";
        //DotNet.invokeMethodAsync('WebApplication1', 'RefreshWindowList',"");
    } catch (e) {

    }
}
function closeSidePanel() {
     document.getElementById("mySidepanel").style.width = "0";
}
var openwindowlist = {};
function open_a_booking(winid,id,guestname) {
    var a_ref = "/Bookings/Bookings/" + id + "/" + guestname;
     a_ref = "/Booking/BookingDetail/" + id + "/" + guestname;
  //  a_ref = "/Booking/BookingMainPage/" + id + "/" + guestname;
   // alert(a_ref);
    openwindowlist[winid] = window.open(a_ref, winid) //, 'location=no,scrollbars=yes,status=no,toolbar=yes,resizable=yes,width=800');
    // below open a popup
    //  openwindowlist[winid] = window.open(a_ref, winid, 'location=no,scrollbars=yes,status=no,toolbar=yes,resizable=yes,width=800');

    console.log(openwindowlist[winid]);
   // DotNet.invokeMethodAsync('WebApplication1', 'AddWindowName', winName);
}

function close_a_booking(winid) {
    var a_win = openwindowlist[winid];
    if (a_win) {
        a_win.close();
        delete openwindowlist[id];
     }
}

function showAlert() {
    //test alert
    if (confirm("Are you sure to change this booking's arrival/departure date and time?"))
    {
        return true;
    }
    else
    {
        return false;
    }
}
async function showAlert2() {
    var a_ok = false;

    await  swal({
        title: "Confirm",
        text: "Are you sure to change this booking's arrival/departure date and time?",
        icon: "info",
        buttons: true,
        //dangerMode: true
    })
        .then((willDelete) => {
            if (willDelete) {
                a_ok = true;
            } else {
                a_ok = false;
            }
        });

    return a_ok;
}
async function gotoRoomSwal() {
    var a_ok = true;

    await swal("Room Number:", {
        content: "input",
    })
        .then((value) => {
            return DotNet.invokeMethodAsync('WebApplication1', 'UpdateMessageCaller', value);
         //return DotNet.invokeMethodAsync('WebApplication1', 'GoDate1', value);
         //  swal(`You typed: ${value}`);
        }).then((message ) => {
            if (message >2) {
                swal("No such room code found.");
            } else {
                a_ok = false;
            }
        });

    return a_ok;
}


function showAlertMove() {
    if (confirm("Are you sure to move this booking to this location?")) { return true; }
    else { return false; }
}
async function showAlertMove2() {
    var a_ok = false;

    await swal({
        title: "Confirm",
        text: "Are you sure to move this booking to this location?",
        icon: "info",
        buttons: true,
        //dangerMode: true
    })
        .then((willDelete) => {
            if (willDelete) {
                a_ok = true;
            } else {
                a_ok = false;
            }
        });
    return a_ok;
}
async function openABookingMessage(aBookingId,aMessage) {
    var a_ok = false;
    var a_id = aBookingId;
    await swal({
        title: "Booking",
        text: aMessage,
        icon: "info",
        buttons: true,
        //dangerMode: true
    })
        .then((aYes) => {
            if (aYes) {
                a_ok = true;
                //open a booking
            } else {
                a_ok = false;
            }
        });
    return a_ok;
}


function showMessage2(a_type) {
    if (a_type == "nomove")
    { swal("Date/Time Conflict with Existing Booking", "This booking is unable to be moved to the new location as it conflicts with an existing booking.", "error"); }
    if (a_type == "nomove2") // the same as nomove message  but a slightly differnt message to tell which checking has stopped the move
    { swal("Conflicts with Existing Booking", "This booking is unable to be moved to the new location as it conflicts with other existing bookings.", "error"); }
    if (a_type == "diagonal")
    { swal("Invalid Move", "This booking is unable to be moved to the new location as its arrival/departure date is changed.", "error"); }

    if (a_type == "noresize")
    { swal("Date/Time Conflict with Existing Booking", "This booking is unable to be changed to the chosen date / time as it conflicts with an existing booking.", "error"); }
    if (a_type == "nodepartmove") {
        swal("Departed / Cancelled Booking", "'Departed' or 'Cancelled' Bookings cannot be moved.", "error"); }
    if (a_type == "noroomtypemove") {
        swal("Invalid", "Sorry, you cannot move to this location!", "error");
    }
    if (a_type == "nodualkey1") {
        swal("Dual Key Booking", "This dual key booking is unable to be changed to the chosen date / time as it conflicts with an existing booking for this dual key " + gstrBookingStockDesc + ".", "error");
    }
    if (a_type == "nodualkey-1") {
        swal("Dual Key Booking", "This dual key booking is unable to be changed as it cannot find all dual key rooms " + gstrBookingStockDesc + "s.", "error");
    }
    if (a_type == "onlybookingmove") {
        swal("Booking Status Only", "Only bookings with status 'Booking' can be moved using the drag and drop functionality.", "error");
    }
    if (a_type == "usemaindualkey") {
        swal("Use Main Dual Key", "Please use the main dual key room to move.", "error");
    }
    if (a_type == "roomnotfound") {
        swal("Room Number/Reference Not Found", "Please check the Room Number/Reference.", "error");
    }
    if (a_type == "nodualkeyadjoining") {
        swal("Dual Key Booking", "The adjoining this dual key has an existing booking. This booking cannot be moved.", "error");
    }

}

function showMessage(a_in) {
   // alert(a_in);
    swal("Date/Time Conflict with Existing Booking", a_in, "error");
}
function refreshPage2() {
    location.reload(true);
}
function showAlertOnly() {
    alert("zok ");
}
function getDimensions() {
    return {
        width: window.innerWidth,
        height: window.innerHeight
    };
}

function getScrollPosition() {
    var el = document.getElementsByClassName("e-table-wrap e-timeline-month-view e-current-panel e-virtual-scroll");
    var xx2 = el.scrollLeft;
    return xx2;
}

function showCoords(event) {
    var x = event.clientX;
    var y = event.clientY;
    var coords = "X coords: " + x + ", Y coords: " + y;
    alert( coords);
}

function isDevice() {
    return /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini|mobile/i.test(navigator.userAgent);
}
//function showAlertCreate() {
//    if (confirm("Would you like to create a booking now?")) { return true; }
//    else { return false; }
//}