﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Pages
{
    public class FetchDataBase : ComponentBase
    {
        public const string Title = "Hello World - Code Behind";
        public bool ShowPopup = false;
        public bool ShowPopup2 = false;

        public void OpenPopup()
        {
            ShowPopup = true;
        }
       public void ClosePopup()
        {
            ShowPopup = false;
        }
        public void OpenPopup2()
        {
            ShowPopup2 = true;
        }
        public void ClosePopup2()
        {
            ShowPopup2 = false;
        }
    }
}
