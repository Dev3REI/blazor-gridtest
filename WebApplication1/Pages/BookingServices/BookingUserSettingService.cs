﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Pages.BookingServices
{

    public class BookingModuleData
    {
        public int Age { get; set; }
        public List<string> OpenWindowNames { get; set; } = new List<string>();
    }

    public class BService
    {
        public static string a_connEncEzy = MyCrypt.EzyConnStr();
        public Guid ServiceId { get; set; }
        public Guid GetGUID()
        {
            return Guid.NewGuid();
        }
        public static BookingUserSettings ff;
        // private BookingUserSettings aBookingUserSettings;
       static public BookingUserSettings GetBookingUserSettings()
        {
          //  a_connEncEzy = "Data Source=sql\\reisql12;Initial Catalog=turtlebeachrei;Persist Security Info=True;User ID=sa;Password=Savvy123";
            DataTable dt_defaults = new DataTable();
            var rowFound = 0;

            using (SqlConnection conn = new SqlConnection(a_connEncEzy))
            {
                //////////////////////
                conn.Open();
                var aSQL = ""; // TOP (80) <<<<<<< -*****************************
                aSQL = " SELECT BookingStockDescription,isnull(DisplayDescriptionOnBookingGrid,0) DisplayDescriptionOnBookingGrid,isnull(UseNettAccommMethod,0) UseNettAccommMethod FROM TBL_Defaults WHERE ID = 1 ";

                using (SqlDataAdapter adapter1 = new SqlDataAdapter(aSQL, conn))
                {
                    adapter1.Fill(dt_defaults);
                    rowFound = dt_defaults.Rows.Count;
                    var ss1 = 3;
                    ss1 = ss1 + 4;
                }
            }
            return new BookingUserSettings()
            {
                gstrBookingStockDesc = rowFound > 0 ? dt_defaults.Rows[0]["BookingStockDescription"].ToString() : "", //"Room"
                gUseDescOnBooking = rowFound > 0 ? (bool)(dt_defaults.Rows[0]["DisplayDescriptionOnBookingGrid"] ?? false) : false
            };

        }
    }

    public class BookingUserSettings
    {
        public int Id { get; set; }
        public string gstrBookingStockDesc { get; set; }
        public bool gUseDescOnBooking { get; set; }
        public bool gUseBookingNetAccom { get; set; }
    }
    public class BookingGridDisplaySetting
    {
        public int Id { get; set; }
        public string gBookingRoomCode_Width { get; set; } //0
        public string gBookingRow_Height { get; set; }
        public string gBookingCol_Width { get; set; }
        public string gBookingGrid_Style { get; set; }
        public string gBookingGrid_Sort { get; set; }
        public string gBookingGrid_FilterStyle { get; set; } //5
        public string gBookingGrid_FilterName { get; set; } // maybe ne need this but leave it in case (this is total combination of all fields below) and saved to "update tbl_useroptions  set BookingGridRoomFilterType ="
        public string pBookingGrid_FilterName1 { get; set; }
        public string pBookingGrid_FilterName2 { get; set; }
        public string pBookingGrid_FilterBookingType { get; set; }
        public string pBookingGrid_FilterAgent { get; set; }
        public string pBookingGrid_CollpaseRows { get; set; }
        public string pBookingGrid_ShowAgent { get; set; }
        public string pBookingGrid_ShowDualkeyLink { get; set; }
        public string pBookingGrid_TimePeriodBy { get; set; }
    }

}
