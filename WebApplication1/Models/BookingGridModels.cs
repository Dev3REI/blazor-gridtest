﻿using System;
using System.Collections.Generic;
using System.Text;

namespace REIMaster.Interfaces.Models.Bookings
{
    public class BookingGridDisplaySetting
    {
        public int Id { get; set; }
        public string gBookingRoomCode_Width { get; set; } //0
        public string gBookingRow_Height { get; set; }
        public string gBookingCol_Width { get; set; }
        public string gBookingGrid_Style { get; set; }
        public string gBookingGrid_Sort { get; set; }
        public string gBookingGrid_FilterStyle { get; set; } //5
        public string gBookingGrid_FilterName { get; set; } // maybe ne need this but leave it in case (this is total combination of all fields below) and saved to "update tbl_useroptions  set BookingGridRoomFilterType ="
        public string pBookingGrid_FilterName1 { get; set; }
        public string pBookingGrid_FilterName2 { get; set; }
        public string pBookingGrid_FilterBookingType { get; set; }
        public string pBookingGrid_FilterAgent { get; set; }
        public string pBookingGrid_CollpaseRows { get; set; }
        public string pBookingGrid_ShowAgent { get; set; }
        public string pBookingGrid_ShowDualkeyLink { get; set; }
        public string pBookingGrid_TimePeriodBy { get; set; }
    }
}
